#include <iostream>
#include <string>
#include <map>
#include <vector>
using namespace std;

typedef map<std::string,bool> strmap;
typedef map<std::string, strmap> str2map;
typedef map<string, strmap>::iterator iter_map;
str2map Z;

struct pair_string{
  string s1,s2;
};

int main() {
    //freopen("1.in","r",stdin); //freopen("output.txt","w",stdout);
    map<string, int> M; int n; string s1,s2,s3,s4("Isenbaev");
    vector<string> query;
    cin>>n;
    for(int i=0;i<n;++i){
      cin>>s1>>s2>>s3;
      Z[s1][s2] = Z[s2][s1] = 1;
      Z[s2][s3] = Z[s3][s2] = 1;
      Z[s1][s3] = Z[s3][s1] = 1;
      M[s1] = M[s2] = M[s3] = 0;
      if ((s1==s4)||(s2==s4)||(s3==s4)) M["Isenbaev"] = 1;
    }

    //M["Isenbaev"] = 1; // ответ +1
    query.push_back("Isenbaev");

    int k=-1;
    while (1) {
      ++k; if (k==query.size()) break;
      for(map<string, bool>::iterator it = Z[query[k]].begin(); it != Z[query[k]].end(); it++) { // цикл, по всем связанным с вершиной
        if ((*it).second) { // если связана
          if (M[(*it).first]==0) {
            query.push_back((*it).first); // ложим в очередь
            M[(*it).first] = M[query[k]]+1;
          }
        }
      }
    }

    for(map<string, int>::const_iterator it = M.begin(); it != M.end(); it++) {
      cout<<(*it).first<<' ';
      if ((*it).second == 0) cout<<"undefined\n"; else cout<<(*it).second - 1<<'\n';
    }

    return 0;
}
