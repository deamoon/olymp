#include <iostream>
using namespace std;

struct friends{
  int m, num, p;
};

int M[102][102], n;
friends F[102];

void floyd() {
  for (int k=1; k<=n; ++k)
      for (int i=1; i<=n; ++i)
          for (int j=1; j<=n; ++j)
              if ( M[i][k] + M[k][j] < M[i][j] ) M[i][j] = M[i][k] + M[k][j];
  for (int i=1; i<=n; ++i) M[i][i] = 0;
}

int main() {
    //freopen("1.in","r",stdin);
    int m, l, z, k, q[102];
    int w[102], ww[102]; // ww - пересадка, если 1
    for (int i=0; i<102; ++i) w[i] = ww[i] = 0;
    for (int i=0; i<102; ++i)
        for (int j=0; j<102; ++j)
            M[i][j] = 100*1000;

    cin>>n>>m;
    for (int i=1; i<=m; ++i) {
        cin>>l; if (l==1) while (1) {};
        for (int j=1; j<=l; ++j) {
            cin>>z; q[j] = z;
            if (w[z]) ww[z] = 1;
            w[z] = 1;
        }
        for (int j=1; j<=l-1; ++j) M[q[j+1]][q[j]] = M[q[j]][q[j+1]] = 0;
    }
    for (int i=1; i<=n; ++i) {
        if (ww[i]) {
            for (int j=1; j<=n; ++j) {
                if (M[i][j] == 0) M[i][j] = 4;
            }
        }
    }
    cin>>k;
    for (int i=1; i<=k; ++i) {
        cin>>F[i].m>>z>>F[i].p; F[i].num = z;
    }
    floyd();

    int min = 100000-1, s, res, tmp;
    for (int i=1; i<=n; ++i) {
        s = 0;
        for (int j=1; j<=k; ++j) {
            if ( (F[j].p == 1) && (M[F[j].num][i] < 100000) ) { continue; }
            if ( ww[F[j].num] ) tmp = M[F[j].num][i]; else
                if (i!=F[j].num)  tmp = M[F[j].num][i] + 4; else tmp = 0;
            if ( tmp > F[j].m ) { s = 100000; break; }
            s += tmp;
        }
        if (s < min) { min = s; res = i; }
    }

    if ( min >= 100000-1 ) cout<<'0'; else cout<<res<<' '<<min;

    /*cout<<'\n';
    for (int i=1; i<=n; ++i) {
        for (int j=1; j<=n; ++j)
            cout<<M[i][j]<<' ';
        cout<<'\n';
    }*/
    //cout<<ww[4];

    return 0;
}
