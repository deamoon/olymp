#include <iostream>
#include <string>
#include <map>
using namespace std;
unsigned long long int t=0;
map<string, string> qw;

string Reverse(string &s){
    string s1;
    for(int i=s.length()-1;i>=0;--i) s1.push_back(s[i]);
    return s1;
}

string toString(size_t n){
    string s;
    while (n!=0) {
        s.push_back((char)(n%10+(int)('0')));
        n=n/10;
    }
    return Reverse(s);
}
string min(string &s1,string &s2,string &s3){
    size_t a=s1.length(),b=s2.length(),c=s3.length();
    if ((a<b) && (a<c)) return s1; else
      if ((b<a) && (b<c)) return s2; else
        return s3;
}

string Repres(string &s){

    if(qw.find(s) != qw.end()) return qw[s];

    size_t n = s.length();
    if (n<=4) { qw[s]=s; return s;}
    string s1,s2=s,s3,s4=s; t++;
    for(size_t i=1;i<n;++i) {
        if ((n%i)==0) {
            bool q=1;
            for(size_t j=i;j<n;j+=i) {
                if (s.substr(j,i)!=s.substr(0,i)) {q=0; break;}
            }
            if (q) {
                s3=s.substr(0,i);
                s1=toString(n/i)+'(' + Repres(s3)+')';
                if ((s1.length()<s2.length()) && (s1.length()<s.length())) s2=s1;
            }
        }
    }
    if (s2.length()==n) {
        string w1,w2,w3;
        for(size_t i=1;i<n;++i) {
            w1=s.substr(0,i); w2=s.substr(i,n-i);
            w3=Repres(w1)+Repres(w2);
            if (w3.length()<s2.length()) s2=w3;
        }
    }
    qw[s4]=s2;
    return s2;
}

int main(){
    string s;
    getline(cin,s);
    //s="NNEERCYESYESYESNEERCYESYESYES";
    //s="AAAAAAAAAABABABCCD"; //t++;
    //s="ababababnmnmnmnmnmnmnm";
    cout<<Repres(s)<<'\n';
    return 0;
}
