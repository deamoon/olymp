#include <iostream>
using namespace std;

int n,a[100][100],b[100],q=1;

void exx(int r){
  if (!q) return;
  if (r==-1) cout<<"-1"; else
    for(int i=1;i<=n;++i) cout<<b[i];
}

void DFS(int ver, int color){
  b[ver] = color;
  for(int i=1;i<=n;++i){
    if (i==ver) continue;
    if ((color==b[i]) && (a[i][ver])) { exx(-1); q=0; return; }
    if ((b[i]==-1) && (a[i][ver])) DFS(i,1-color);
  }
}

int main()
{
    //freopen("1.in","r",stdin); //freopen("output.txt","w",stdout);
    int k;
    cin>>n;
    for(int i=0;i<=99;++i) for(int j=0;j<=99;++j) a[i][j]=0;
    for(int i=0;i<=99;++i) b[i]=-1;
    for(int i=1;i<=n;++i) {
      while(1){
        cin>>k;
        if (k==0) break;
        a[k][i] = a[i][k] = 1;
      }
    }
    b[1]=0;
    DFS(1,0);
    if (q) exx(0);
    return 0;
}
