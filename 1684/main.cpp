/*
  Задача 1684. Последнее слово Джека
  http://acm.timus.ru/problem.aspx?space=1&num=1684
*/

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

std::vector<int> zFunction (const std::string &str) {
	int length = (int) str.length();
	std::vector<int> zFunVector(length);
	for (int currentIndex = 1, blockBegin = 0, blockEnd = 0; currentIndex < length; ++currentIndex) {
		if (currentIndex <= blockEnd) {
			int distToBlockEnd = blockEnd - currentIndex + 1;
			int distToBlockBegin = currentIndex - blockBegin;
			zFunVector[currentIndex] = std::min(distToBlockEnd, zFunVector[distToBlockBegin]);
		}
		while (currentIndex + zFunVector[currentIndex] < length && 
				str[zFunVector[currentIndex]] == str[currentIndex + zFunVector[currentIndex]]) {
			++zFunVector[currentIndex];
		}
		if (currentIndex + zFunVector[currentIndex] - 1 > blockEnd) {
			blockBegin = currentIndex;
			blockEnd = currentIndex + zFunVector[currentIndex] - 1;
		}
	}
	return zFunVector;
}

int input(std::string &in1, std::string &in2) {
    std::cin>>in1>>in2;
    if (in1.length() > 0 and in2.length() > 0) return 0; else return 1;
}

void output(const std::string &out){
    if (out.length()>0) {
        std::cout<<"No"<<"\n"<<out;
    } else {
        std::cout<<"Yes";
    }
}

std::string solveProblem(std::string &inFence, std::string &inJack) {
    std::vector<int> zStr = zFunction(inFence + '#' + inJack);
    int shift = inFence.length() + 1; // Номер начала строки inJack
    int balance = zStr[shift]; // Гарантированная длина префикса
    std::string res = "";
    for (int i=shift; i<zStr.size(); ++i) {
        if (zStr[i] > balance) {
            res += ' ';
            balance = zStr[i];
        }
        if (balance <= 0) return "";
        --balance;
        res += inJack[i - shift];
    }
    return res;
}

int main() {
    std::string inFence, inJack, out;

    if (input(inFence, inJack)) return 1;
    out = solveProblem(inFence, inJack);
    output(out);

    return 0;
}


