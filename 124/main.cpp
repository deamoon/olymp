#include <stdio.h>
#include <stdlib.h>

void swap(int * a, int n1, int n2)
{
    int sw = a[n1]; a[n1] = a[n2]; a[n2] = sw;
    return;
}
void sort(int * a, int left, int right)
{
    int cn = a[left + rand() % (right-left+1)], l = left, r = right;
    do{
      while (a[l]<cn) ++l;
      while (a[r]>cn) --r;
      if (l<=r){
        swap(a, l, r);
        ++l; --r;
      }
    }
    while (l <= r);
    if (left < r)  sort(a, left, r);
    if (l < right) sort(a, l, right);
    return;
}

int main(void){
    FILE *in, *out;
    int i=0, *a, len;

    out = fopen("output.txt", "w");
    if(!out) return -1;
    in = fopen("input.txt","r");
    if (!in) {fprintf(out,"ERROR!"); fclose (out); return -1;}

    fscanf(in, "%d", &len);
    a = (int *)malloc(len * sizeof(int));
    for (i = 0; i < len; i++)
      fscanf(in, "%d", &(a[i]));

    sort(a,0,len-1);

    for (i = 0; i < len; i++)
      fprintf(out, "%d ", (a[i]));

    return 0;
}
