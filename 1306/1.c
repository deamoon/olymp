#include <stdio.h>

int max(int a, int b){
    if ((a>b)) return a; else return b;
}

void siftUp(int *a, int j){
    int t;
    while ((j>1) && (a[j]>a[j/2])) {
        t = a[j/2]; a[j/2] = a[j]; a[j] = t;
        //swap(a[j/2],a[j]);
        j=j/2;
    }
}

void siftDown(int *a, int n, int j, int k){
    int t;
    while (k+1>2*j) { //Выполнять пока потомки существуют
        if (2*j+1>k) { //Если потомок один (значит потомок - это посл-ий элемент)
            if ((a[2*j]>a[j])) { //И потомок больше
                //swap(a[j],a[2*j]);
                t = a[j]; a[j] = a[2*j]; a[2*j] = t;
            }
            break;
        } else {
            if ((max(a[2*j],a[2*j+1])>a[j])) {//Родитель не максимальный
                if ((a[2*j]>a[2*j+1])) {
                    //swap(a[j],a[2*j]);
                    t = a[j]; a[j] = a[2*j]; a[2*j] = t;
                    j=2*j;
                }//Меняем родителя с максимальным потомком
                    else {
                        //swap(a[j],a[2*j+1]);
                        t = a[j]; a[j] = a[2*j+1]; a[2*j+1] = t;
                        j=2*j+1;
                        }
            } else break;
        }
    }
}

void hSort(int *a,int n){
    int k=n; int t, i;
    for (i=2;i<=n;i++) siftUp(a,i);//Создаем кучу
    for (i=1;i<=n-1;i++) { //Hsortим
        //swap(a[1],a[k]);
        t = a[1]; a[1] = a[k]; a[k] = t;
        k--;
        siftDown(a,n,1,k);
    }
}

int main() {

    freopen("in.txt", "r", stdin);
    int a[190000], n, i;
    scanf("%d",&n);

    int p = 3*n/4;
    if ( (p%2) != (n%2) ) ++p;

    for(i=1;i<=p;++i) {
      scanf("%d", &a[i]);
    }

    hSort(a,p);

    int l = 0, r = p+1;
    for(i=p+1;i<=n;++i) {
      if (i%2==1) scanf("%d", &a[++l]); else scanf("%d", &a[--r]);
    }
    //if (p%2!=0 && n%2==1) {a[--r] = 1024*1024*1024*2-1;}
    //if (p%2!=0 && n%2!=1) {a[++l] = -1;}

    hSort(a,p);

    if (n%2==0) {
      printf("%.1f\n", (double)(a[p/2]+a[p/2+1])/2.0);
    } else {
      printf("%d\n", a[(p+1)/2]);
    }

    return 0;
}
