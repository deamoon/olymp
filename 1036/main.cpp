#include <iostream>
#include <vector>
using namespace std;

vector<short int> sum(vector<short int> a, vector<short int> b){
  vector<short int> c;
  if (a.size()>b.size()) {
    for(size_t i=b.size();i<a.size();++i) b.push_back(0);
  } else {
    for(size_t i=a.size();i<b.size();++i) a.push_back(0);
  }
  int p=0;
  for(size_t i=0;i<max(a.size(),b.size());++i) {
    c.push_back((a[i]+b[i]+p)%10);
    p=(a[i]+b[i]+p)/10;
  }
  if (p!=0) c.push_back(p);
  return c;
}

vector<short int> prInt(vector<short int> &a, int b){
  vector<short int> c;
  int p=0;
  for(size_t i=0;i<a.size();++i) {
    c.push_back((a[i]*b+p)%10);
    p=(a[i]*b+p)/10;
  }
  if (p!=0) c.push_back(p);
  return c;
}

vector<short int> pr(vector<short int> &a, vector<short int> &b){
  vector<short int> c,d;
  c.push_back(0);
  for(size_t i=0;i<b.size();++i) {
    d=prInt(a,b[i]);
    for(size_t k=0;k<i;++k) d.push_back(0);
    for(int k=d.size()-1-i;k>=0;--k) d[k+i]=d[k];
    for(size_t k=0;k<i;++k) d[k]=0;
    c=sum(c,d);
  }
  return c;
}

void write(vector<short int> &a){
    for(int i=a.size()-1;i>=0;--i) cout<<a[i];
}

int main()
{
    int n,s;
    vector<short int> a[51][1001],res;
    cin>>n>>s;
    if ((s%2==0)&&(s/2<=9*n)) {
      s=s/2;
      for(int i=0;i<=50;++i) for(int j=0;j<=1000;++j) a[i][j].push_back(0);
      for(int i=1;i<=n;++i) a[i][0][0]=1;
      for(int i=0;i<=9;++i) a[1][i][0]=1;

//write(a[3][0]); cout<<'\n';

      for(int i=2;i<=n;++i) {
        for(int j=1;j<=s;++j) {
          for(int k=0;k<=9;++k) {
            a[i][j]=sum(a[i][j],a[i-1][j-k]);
            if ((j-k)==0) break;
          }
          //if ((j-10)>=0) a[i][j]=raz(sum(a[i][j-1],a[i-1][j]),a[i-1][j-10]); else a[i][j]=sum(a[i][j-1],a[i-1][j]);
        }
        //write(a[3][0]); cout<<'\n';
      }
    res=pr(a[n][s],a[n][s]);
    write(res);

    } else cout<<'0';

 /*   cout<<'\n';
    for(int i=1;i<=n;++i) {
      for(int j=0;j<=s;++j) {
      write(a[i][j]);cout<<' ';
    }cout<<'\n';}

    write(a[2][4]);
*/
    return 0;
}
