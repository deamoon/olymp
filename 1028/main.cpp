#include <iostream>
#include <fstream>
using namespace std;
const int nlist=32768;
int a[65536];

void update(int x){
    int t = x+nlist;
    while (t>=1) {
      ++a[t]; t=t/2;
    }
}

int RSQ(int l, int r){
    l+=nlist; r+=nlist; int res=0;
    while (l<=r){
      if (l%2==1) { res+=a[l]; l=(l+1)/2; } else l=l/2;
      if (r%2==0) { res+=a[r]; r=(r-1)/2; } else r=r/2;
    }
    return res;
}

int main()
{
    int b[15000];
    for (int i=0;i<65536;++i) a[i]=0; for (int i=0;i<15000;++i) b[i]=0;
    //ifstream(f); f.open("1.in");
    int x,y,n;
    cin>>n;
    for(int i=1;i<=n;++i){
        cin>>x>>y;
        ++b[RSQ(0,x)];
        update(x);
    }
    //f.close();
    for(int i=0;i<n;++i) cout<<b[i]<<'\n';
    return 0;
}
