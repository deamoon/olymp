#include <iostream>
#include <cmath>
#include <ctime>
#include <cstdlib>
using namespace std;

struct node {
    int x, y, sz, low_bound, add;
    node* l;
    node* r;
    node(void){}
};

typedef node* pnode;

pnode root;
int n, m;
int gsz(pnode t) {
  return (t ? t->sz : 0);
}

void upd(pnode &t) {
    if (t) {
      t->sz = gsz(t->l) + gsz(t->r) + 1;
      t->low_bound = t->add + gsz(t->l);
      if (t->l)
          t->l->add = t->add;
      if (t->r)
          t->r->add = t->low_bound + 1;
      t->add = 0;
    }
}

void split(pnode t, int x, pnode &l, pnode &r) {
    if (!t) l = r = NULL;
    else {
      if (t->x > x) {
        split(t->l, x, l, t->l);
        upd(t);
        r = t;
      }else{
        split(t->r, x, t->r, r);
        upd(t);
        l = t;
      }
    }
}

pnode merge(pnode l, pnode r) {
    upd(l); upd(r);
    if (!l) return r;
    if (!r) return l;
    if (l->y < r->y) {
      l->r = merge(l->r, r);
      upd(l);
      return l;
    }else{
      r->l = merge(l, r->l);
      upd(r);
      return r;
    }
}

void insert(pnode &t, int x, int y) {
    if (!t || y > t->y) {
      pnode tmp = new node();
      split(t, x, tmp->l, tmp->r);
      t = tmp;
      t->x = x;
      t->y = y;
      t->add = 0;
      upd(t);
    }
    else
      if (x < t->x){
        insert(t->l, x, y);
        upd(t);
      }else{
        insert(t->r, x, y);
        upd(t);
      }
}

int ALB(int x) {
    int m, l1, r1;
    bool r = true;
    if (!root) return x;
    pnode t = root;
    upd(t);
    l1 = x;
    r1 = x + t->sz;
    while (r) {
      upd(t);
      m = t->x - t->low_bound - 1;
      if (x > m){
        l1 = t->x + 1;
        if (!t->r) r = false; else t = t->r;
      }else{
        r1 = t->x - 1;
        if (!t->l) r = false; else t = t->l;
      }
    }
    if (x > m) return l1 + x - m - 1;
      else return r1 + x - m;
}

int main() {
    cin>>n>>m;
    char c; int x;
    srand(time(0));
    for(int i=0; i<m; i++) {
      cin>>c>>x;
      if (c == 'D')
        insert(root, ALB(x), rand()|(rand() << 16));
      else
        cout << ALB(x) << '\n';
    }
    return 0;
}
