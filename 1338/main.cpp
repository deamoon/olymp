#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <stdio.h>
using namespace std;

int W, H, M;
char A[503][503];
bool B[503][503];
vector < pair < int, int > > point;
queue < pair < int, int > > qt;
int res[250005][6]; // Север, Юг, Запад, Восток

void took(int i, int j, int k) {
    for (int n=1;n<=point.size()-1;++n) {
        if (point[n].first==i && point[n].second==j) {
            ++res[n][k];
            break;
        }
    }
}

void BFS(int i, int j) {
    for (int i=H;i>=1;--i) {
        for (int j=1;j<=W;++j) {
            B[i][j] = 0;
        }
    }
    while (!qt.empty()) qt.pop();
    qt.push(make_pair(i, j));
    i = qt.front().first; j = qt.front().second;
    A[i][j] = '#';
    while (!qt.empty()) {
      i = qt.front().first; j = qt.front().second;
      qt.pop();
      if (A[i+1][j]=='o') {
          took(i+1, j, 1);
          A[i+1][j] = '#';
      }
      if (A[i-1][j]=='o') {
          took(i-1, j, 2);
          A[i-1][j] = '#';
      }
      if (A[i][j-1]=='o') {
          took(i, j-1, 3);
          A[i][j-1] = '#';
      }
      if (A[i][j+1]=='o') {
          took(i, j+1, 4);
          A[i][j+1] = '#';
      }
      if (A[i+1][j]!='.' && !B[i+1][j])
        qt.push(make_pair(i+1, j));
        B[i+1][j] = 0;
      if (A[i-1][j]!='.' && !B[i-1][j])
        qt.push(make_pair(i-1, j));
        B[i-1][j] = 0;
      if (A[i][j-1]!='.' && !B[i][j-1])
        qt.push(make_pair(i, j-1));
        B[i][j-1] = 0;
      if (A[i][j+1]!='.' && !B[i][j+1])
        qt.push(make_pair(i, j+1));
        B[i][j+1] = 0;
    }
    for (int n=1;n<=point.size()-1;++n) {
      A[point[n].first][point[n].second] = 'o';
    }
}

int main() {
    int n;
    #ifndef ONLINE_JUDGE
      freopen("1.txt", "r", stdin);
    #endif
    cin>>W>>H;
    point.push_back(make_pair(-1, -1));
    for (int i=1;i<=H;++i) {
        for (int j=1;j<=W;++j) {
            do {
                cin>>A[i][j];
            } while (A[i][j]!='.' && A[i][j]!='#' && A[i][j]!='o');
        }

    }
    for (int i=H;i>=1;--i) {
        for (int j=1;j<=W;++j) {
            B[i][j] = 1;
            if (A[i][j]=='o') {
                point.push_back(make_pair(i,j));
            }
        }
    }

    for (int i=1;i<=H;++i) {
        A[i][W+1] = A[i][0] = '.';
        A[0][i] = A[H+1][i] = '.';
    }
    for (int i=1;i<=20;++i) {
      for (int j=1;j<=4;++j) {
          res[i][j] = 0;
      }
    }

    cin>>M;

    if ((M>0) && (point.size()>1)) {
        for (int i=1;i<=M;++i) {
            cin>>n;
            BFS(point[n].first, point[n].second);
            //printf("Experiment #%d: North: %d, South: %d, East: %d, West: %d\n", i, res[n][1], res[n][2], res[n][3], res[n][4]);
        }
    }
    /*for (int i=1;i<=point.size()-1;++i) {
        for (int j=1;j<=4;++j) {
            cout<<res[i][j]<<" ";
        }
        cout<<endl;
    }*/
    return 0;
}
