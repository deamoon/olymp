#include <iostream>
#include <vector>
#include <set>

using namespace std;

const int N = 100*1001;

struct DSU {
    int parent[N];
    int size[N];

    void make_set(int v) {
        parent[v] = v;
        size[v] = 1;
    }

    void print(int n) {
        for (int i=1;i<=n;++i) {
            cout<<i<<" : "<<find_set(i)<<endl;
        }
    }

    int size_set(int v) {
        return size[find_set(v)];
    }

    int find_set(int v) {
        if (v == parent[v]) {
            return v;
        }
        return parent[v] = find_set(parent[v]);
    }

    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            if (size[a] < size[b]) {
                swap (a, b);
            }
            parent[b] = a;
            size[a] += size[b];
        }
    }
};

vector< vector<int> > G;
int used[N];
DSU dsu;
int parent[N];
set< pair<int, int> > circle;
set<int> black_list;
int V;
int xit[N];

int DFS3(int r) {
    used[r] = 1;
    int g = 0;
    for (int i = 0; i < G[r].size(); ++i) {
        bool flag = dsu.find_set(r) != dsu.find_set(G[r][i]);
        if (!flag) {
            used[G[r][i]] = 1;
        }
        if (!used[G[r][i]] && flag) {
            int k = DFS3(G[r][i]);
            g ^= (k + 1);
            //cout<<"DFS "<<G[r][i]<<" = "<<k<<endl;
        }
    }
    return g ^ ((xit[r]/2) % 2);
}

void DFS2(int r) {
    used[r] = 1;
    int size;
    size = dsu.size_set(r);
    int kol = 0;

    for (int i = 0; i < G[r].size(); ++i) {
        if (!used[G[r][i]]) {
            if (dsu.find_set(r) == dsu.find_set(G[r][i])) {
                kol += 1;
                for (int j = 0; j < G[G[r][i]].size(); ++j) {
                    G[r].push_back(G[G[r][i]][j]);
                }
                G[G[r][i]].clear();
            } else {
                DFS2(G[r][i]);
            }
        }
    }

    //for (int i = kol + 1; i <= size; ++i) {
    //    V += 1;
        //G.push_back(vector<int>());
        //G[r].push_back(V);
    //}
}

void DFS1(int r) {
    used[r] = 1;
    for (int i = 0; i < G[r].size(); ++i) {
        if (!used[G[r][i]]) {
            parent[G[r][i]] = r;
            DFS1(G[r][i]);
        } else {
            if (G[r][i] == parent[r]) {
                continue;
            }
            if (circle.count(make_pair(r, G[r][i]))) {
                continue;
            }

            circle.insert(make_pair(r, G[r][i]));
            circle.insert(make_pair(G[r][i], r));

            int p = r;
            int old;
            do {
                old = p;
                p = parent[p];

                circle.insert(make_pair(old, p));
                circle.insert(make_pair(p, old));

                dsu.union_sets(G[r][i], p);
            } while (p != G[r][i]);

            dsu.union_sets(r, G[r][i]);

        }
    }
}

int main() {

    #ifndef ONLINE_JUDGE
        freopen("1.in", "r", stdin);
    #endif

    int a, n, m, r, k, old;
    cin>>n>>m>>r;
    G.resize(n+1);
    V = n;

    for (int i=0;i<m;++i) {
        cin>>k;
        cin>>a;
        old = a;
        for (int j=1;j<k;++j) {
            cin>>a;
            G[old].push_back(a);
            G[a].push_back(old);
            old = a;
        }
    }

    for (int i=1;i<=n;++i) {
        dsu.make_set(i);
    }
    parent[r] = r;

    for (int i=0;i<N;++i) {
        used[i] = 0;
    }
    DFS1(r);

    for (int i=0;i<N;++i) {
        used[i] = 0;
        xit[i] = 0;
    }
    DFS2(r);


    for (int i=1;i<G.size();++i) {
        for (int j=0;j<G[i].size();++j) {
            if (dsu.find_set(i) == dsu.find_set(G[i][j])) {
                xit[i] += 1;
            }
        }
    }

    //for (int i=0;i<n;++i) {
    //    cout<<"xit "<<i<<" = "<<xit[i]<<endl;
    //}

    for (int i=0;i<N;++i) {
        used[i] = 0;
    }
    int res = DFS3(r);

    //cout<<res<<endl;
    if (res == 0) {
        cout<<"Second";
    } else {
        cout<<"First";
    }

    //cout<<dsu.find_set(3)<<" "<<dsu.find_set(1)<<endl;
    //dsu.print(n);
    //cout<<endl;
    //for (int i=1;i<G.size();++i) {
    //    cout<<i<<" : ";
    //    for (int j=0;j<G[i].size();++j) {
    //        cout<<G[i][j]<<" ";
    //    }
    //    cout<<endl;
    //}

    return 0;
}













