#include <iostream>
#include <vector>
#include <math.h>
#include <iomanip>
using namespace std;

bool qq = 0;
int a[101][101],n,b[101],s,maxx = -1;

void exx(int r){
  if (qq) return;
  if (r==0) cout<<"NO"; else cout<< "YES";
  qq=1;
}

void DFS(int ver,int pred){
  if (qq) return;
  b[ver] = 0; // серая
  for(int i=1;i<=n;++i){
    if (i==pred) continue;
    if ((a[i][ver]!=0) && (b[i]==0)) { exx(1); return; } // если цикл
    if ((a[i][ver]!=0) && (b[i]==-1)) DFS(i, ver);
  }
  b[ver] = 1; // черная
}

void DFS2(int ver, int res){
  if (qq) return;
  b[ver] = 0;
  for(int i=1;i<=n;++i){
    if ((a[i][ver]!=0) && (b[i]==-1)) DFS2(i,res+a[i][ver]);
  }
  if (res>=maxx) {
    maxx = res;
    if (maxx>=s) { exx(1); return; }
  }
}

int main()
{
    freopen("1.in","r",stdin);  //freopen("output.txt","w",stdout);
    int m,p,q,r;
    cin>>n>>m>>s;

    for(int i=1;i<=100;++i) for(int j=1;j<=100;++j) a[i][j]=0;

    for(int i=1;i<=m;++i){
      cin>>p>>q>>r;
      if (p==q) exx(1); // петли
      if (a[p][q]!=0) exx(1); // кратные ребра
      a[p][q] = a[q][p] = r;
    }
    if (qq) return 0;

    for(int i=1;i<=n;++i) b[i] = -1; // -1 - белая, 0 - серая, 1 - черная

    for(int i=1;i<=n;++i){
      if (b[i]==-1) DFS(i,-1);
    }

    if (qq) return 0;

    //for(int j=1;j<=n;++j){

    for(int i=1;i<=n;++i){
      for(int j=1;j<=n;++j) b[j] = -1;
      DFS2(i,0);
    }
    //}

    if (!qq) exx(0);
    return 0;
}
