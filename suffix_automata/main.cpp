#include <iostream>
#include <map>
#include <vector>

using namespace std;

class SuffixAutomata {
    typedef map<char, int> nodeCharMap;

    static const int INIT_STATE = -1;
    static const int ALPHABET = 26;

    struct State {
        int len;
        int link;
        int go[ALPHABET];
        State() : len(0), link(INIT_STATE) {
            for (char alpha = 0; alpha < ALPHABET; ++alpha) {
                go[alpha] = INIT_STATE;
            }
        }
    };

    int lastState;
    vector<State> state;

    public:

        void buildAutomata(const string &text) {
            state.clear();
            state.push_back(State());
            lastState = 0;
            for (int currentIndex = 0; currentIndex < text.length(); ++currentIndex) {
                addNextSymbol(text[currentIndex]);
            }
        }

        string findCommonMaxSubstring(const string &str) {
            int currentState = 0;
            int currentLen = 0;
            int maxLen = 0;
            int endStringWithMaxLen = 0;

            for (int currentIndex = 0; currentIndex < str.length(); ++currentIndex) {
                char currentChar = str[currentIndex] - 'A';
                while (currentState && state[currentState].go[currentChar] == INIT_STATE) {
                    currentState = state[currentState].link;
                    currentLen = state[currentState].len;
                }
                if (state[currentState].go[currentChar] != INIT_STATE) {
                    currentState = state[currentState].go[currentChar];
                    ++currentLen;
                }
                if (currentLen > maxLen) {
                    maxLen = currentLen;
                    endStringWithMaxLen = currentIndex;
                }
            }
            return str.substr(endStringWithMaxLen - maxLen + 1, maxLen);
        }

        void addNextSymbol(char symbol) {
            symbol -= 'A';
            int newState = state.size();
            state.push_back(State());
            state[newState].len = state[lastState].len + 1;

            int currentState = lastState;
            while (currentState != INIT_STATE && state[currentState].go[symbol] == INIT_STATE) {
                state[currentState].go[symbol] = newState;
                currentState = state[currentState].link;
            }

            if (currentState == INIT_STATE) {
                state[newState].link = 0;
            } else {
                int achieveState = state[currentState].go[symbol];
                if (state[currentState].len + 1 == state[achieveState].len) {
                    state[newState].link = achieveState;
                } else {
                    int clone = state.size();
                    state.push_back(State());

                    state[clone].len = state[currentState].len + 1;
                    for (char alpha = 0; alpha < ALPHABET; ++alpha) {
                        state[clone].go[alpha] = state[achieveState].go[alpha];
                    }

                    state[clone].link = state[achieveState].link;

                    while (currentState != INIT_STATE && state[currentState].go[symbol] == achieveState) {
                        state[currentState].go[symbol] = clone;
                        currentState = state[currentState].link;
                    }

                    state[achieveState].link = state[newState].link = clone;
                }
            }

            lastState = newState;
        }

};

void readInput(string &str1, string &str2) {
    int length;
    cin >> length;
    cin >> str1;
    cin >> str2;
}

void writeOutput(const string &commonMaxSubstring) {
    cout << commonMaxSubstring;
}

int main() {
    string str1, str2;

    //freopen("1.in", "r", stdin);
    readInput(str1, str2);

    SuffixAutomata a;
    a.buildAutomata(str1);

    writeOutput(a.findCommonMaxSubstring(str2));

    return 0;
}
