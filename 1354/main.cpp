/*
  Задача 1354. Палиндром. Он же палиндром
  http://acm.timus.ru/problem.aspx?space=1&num=1354
*/

#include <iostream>
#include <string>
#include <vector>

std::vector<int> prefixFunction(const std::string &str) {
	int length = (int) str.length();
	std::vector<int> prefixFun(length);
	for (int currentIndex = 1; currentIndex < length; ++currentIndex) {
		int prefixLength = prefixFun[currentIndex - 1];
        char currentChar = str[currentIndex];
		while (prefixLength > 0 && str[currentIndex] != str[prefixLength])
			prefixLength = prefixFun[prefixLength - 1];
		if (currentChar == str[prefixLength]) ++prefixLength;
		prefixFun[currentIndex] = prefixLength;
	}
	return prefixFun;
}

int input(std::string &in) {
    std::cin>>in;
    if (in.length() > 0) return 0; else return 1;
}

void output(const std::string &out){
    std::cout<<out;
}

std::string solveProblem(std::string &in) {
    char first = in[0];
    in.erase(0, 1); // Удаляем первый символ из строки

    std::string reverse_in = std::string(in.rbegin(), in.rend());
    std::string str = reverse_in + '#' + in;
    std::vector<int> prefixStr = prefixFunction(str);
    int end = prefixStr.back(); // Последнее значение префикс-функции

    return first + in + std::string(in.rbegin() + end, in.rend()) + first;
}

int main() {
    std::string in, out;

    if (input(in)) return 1;
    out = solveProblem(in);
    output(out);

    return 0;
}


