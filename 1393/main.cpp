#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <stdio.h>
using namespace std;

const int maxlen = 500001+3;
const int alphabet = 256;

int pn[maxlen], cn[maxlen];
int p[maxlen], cnt[maxlen], c[maxlen];
int classes;

void print(int n) {
    cout<<"\n";
    for (int i=0;i<n;++i) {
        cout<<p[i]<<" ";
    }
    cout<<"\n";
}

void buildSufMas(vector<char> &s) {
    int n = s.size();
    memset (cnt, 0, alphabet * sizeof(int));
    for (int i=0; i<n; ++i)
        ++cnt[s[i]];
    for (int i=1; i<alphabet; ++i)
        cnt[i] += cnt[i-1];
    for (int i=0; i<n; ++i)
        p[--cnt[s[i]]] = i;
    c[p[0]] = 0;
    classes = 1;
    for (int i=1; i<n; ++i) {
        if (s[p[i]] != s[p[i-1]])  ++classes;
        c[p[i]] = classes-1;
    }

    for (int h=0; (1<<h)<n; ++h) {
        for (int i=0; i<n; ++i) {
            pn[i] = p[i] - (1<<h);
            if (pn[i] < 0)  pn[i] += n;
        }
        memset (cnt, 0, classes * sizeof(int));
        for (int i=0; i<n; ++i)
            ++cnt[c[pn[i]]];
        for (int i=1; i<classes; ++i)
            cnt[i] += cnt[i-1];
        for (int i=n-1; i>=0; --i)
            p[--cnt[c[pn[i]]]] = pn[i];
        cn[p[0]] = 0;
        classes = 1;
        for (int i=1; i<n; ++i) {
            int mid1 = (p[i] + (1<<h)) % n,  mid2 = (p[i-1] + (1<<h)) % n;
            if (c[p[i]] != c[p[i-1]] || c[mid1] != c[mid2])
                ++classes;
            cn[p[i]] = classes-1;
        }
        memcpy (c, cn, n * sizeof(int));

    }
}

void printSuf(int n, vector<char> &s) {
    for (int j=n;j<s.size()/2;++j) {
        cout<<s[j];
    }
    cout<<"\n\n";
}

vector<int> solve(vector<char> &s, vector<int> &pos) {
    vector<int> rank(pos.size());
    for (int i = 0; i < pos.size(); i++) {
        rank[pos[i]] = i;
    }

    vector<int> ans(pos.size());

    int h = 0;

    for (int i = 0; i < s.size(); i++) {

        if (i >= s.size()/2) continue;

        if (rank[i] == pos.size() - 1) {
            continue;
        }

        int k = pos[rank[i] + 1];

        while (((i + h) != s.size()) && ((k + h) != s.size()) && (s[(i + h)] == s[(k + h)])) {
            h++;
            if (h==s.size()/2) break;
        }
        ans[rank[i]] = h;
        if (h > 0) {
            h--;
        }


    }

    return ans;
}

int main() {
    int n;
    //freopen("1.in", "r", stdin);
    cin>>n;
    char c;
    vector<char> s(2*n);
    for (int i=0;i<n;++i) {
        cin>>c;
        s[i] = s[n+i] = c;
    }
    s[2*n-1] = '#';

    buildSufMas(s);

    for (int i=0;i<s.size();++i) {

    }

    vector<int> pos;

    for (int i=0;i<s.size();++i) {
        if (p[i] < s.size()/2) {
            pos.push_back(p[i]);
        }
    }

    vector<int> ans = solve(s, pos);

    double sum = 0.0;
    for (int i=0;i<ans.size()-1;++i) {
        sum+=ans[i];
    }
    double r = s.size()/2-1;
    printf( "%.3f", (double)( (sum) / r ) );

    return 0;
}
