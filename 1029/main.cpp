#include <iostream>
//#include <fstream>
#include <vector>
using namespace std;
const long int GG=2000000000;
class minHeap{
  private:
    struct list{
      long int x;
      long int y;
      long int key;
    };
    vector<list*> heap;
    void siftDown(size_t n){
      while (true){
        if (2*n+1>=heap.size()) break; // Предков нет
        if (2*n+2<heap.size()) { // 2 предка
          int min;
          if (heap[2*n+1]->key < heap[2*n+2]->key) min=2*n+1; else min=2*n+2;
          if (heap[n]->key > heap[min]->key) {swap(heap[n],heap[min]); n = min; continue; } else break;
        }
        if (heap[n]->key > heap[2*n+1]->key) {swap(heap[n],heap[2*n+1]); n = 2*n+1; continue;} // 1 предок
        break;
      }
    }
  public:
    void siftUp(long int k,long int i,long int j){
      list *l = new list; l->x=i; l->y=j; l->key=k; heap.push_back(l);
      int n = heap.size()-1;
      while (n!=0) {
        if (heap[n]->key<heap[(n-1)/2]->key){
          swap(heap[n],heap[(n-1)/2]);
          n=(n-1)/2;
        } else break;
      }
    }
    void deleteMin(){
      heap[0] = heap[heap.size()-1]; heap.pop_back();
      siftDown(0);
    }
    long int min_x() {return heap[0]->x;}
    long int min_y() {return heap[0]->y;}
    long int min_key() {return heap[0]->key;}
    void write(){
      for(size_t i=0;i<heap.size();++i) cout<<heap[i]->key<<' ';
      cout<<'\n';
    }
};

int main()
{
    long int A[102][502],m,n; bool B[102][502];

    //ifstream(f); f.open("1.in");
    vector<int> res;
    cin>>m>>n;
    for(int i=1;i<=m;++i) for(int j=1;j<=n;++j) { cin>>A[i][j]; B[i][j]=true;}
    //f.close();
    for(int i=0;i<=m;++i) A[i][0]=GG;
    for(int i=0;i<=m;++i) A[i][n+1]=GG;
    for(int j=0;j<=n;++j) A[0][j]=GG;

    minHeap h; int k=0,x,y;
    for(int j=1;j<=n;++j) {h.siftUp(A[1][j],1,j); ++k; B[1][j]=false;}
    while (k!=m*n){
      x = h.min_x(); y = h.min_y(); h.deleteMin();
      if ((y!=1)&&(B[x][y-1])) {A[x][y-1] += A[x][y]; h.siftUp(A[x][y-1],x,y-1); ++k; B[x][y-1]=false;}
      if ((y!=n)&&(B[x][y+1])) {A[x][y+1] += A[x][y]; h.siftUp(A[x][y+1],x,y+1); ++k; B[x][y+1]=false;}
      if ((x!=m)&&(B[x+1][y])) {A[x+1][y] += A[x][y]; h.siftUp(A[x+1][y],x+1,y); ++k; B[x+1][y]=false;}
    }
    x=m; y=1; int min_x,min_y;
    for(int j=2;j<=n;++j) if (A[x][j]<A[x][y]) y=j;
    res.push_back(y); min_x=x; min_y=y; //cout<<x<<' '<<y<<'\n';
    while (x!=1){
      if ((A[x][y-1]<=A[x][y+1])&&(A[x][y-1]<A[x-1][y])) min_y=y-1; else{
        if ((A[x][y+1]<=A[x][y-1])&&(A[x][y+1]<A[x-1][y])) min_y=y+1; else min_x=x-1;
      }
      x=min_x; y=min_y; res.push_back(y);// cout<<x<<' '<<y<<'\n';
    }
    for(int j=res.size()-1;j>=0;--j) cout<<res[j]<<' ';
    /*cout<<'\n';
    for(int i=1;i<=m;++i) {
      for(int j=1;j<=n;++j) cout<<A[i][j]<<' ';
      cout<<'\n';
    }*/
    return 0;
}
