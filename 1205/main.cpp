#include <iostream>
#include <vector>
#include <math.h>
#include <iomanip>
using namespace std;

double absol(double a){
  if (a>=0) return a; else return -a;
}

int main()
{
    freopen("1.in","r",stdin);  //freopen("output.txt","w",stdout);
    double v_p,v_m,x[203],y[203],x1,y1,r[203][203],t;
    double Max_NUM = 2*1000*1000*1000;
    int n,a,b;
    cin>>v_p>>v_m>>n;

    for(int i=1;i<=202;++i) for(int j=1;j<=202;++j) r[i][j]=Max_NUM;

    for(int i=1;i<=n;++i){
      cin>>x1>>y1; x[i]=x1; y[i]=y1;
    }
    while (1) {
      cin>>a>>b;
      if ((a == 0) && (b == 0)) break;
      r[b][a] = sqrt((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b]))/v_m;
      r[a][b] = r[b][a];
    }
    cin>>x1>>y1; x[n+1]=x1; y[n+1]=y1;
    cin>>x1>>y1; x[n+2]=x1; y[n+2]=y1;
    for(int i=1;i<=n+2;++i) for(int j=1;j<=n+2;++j) {
        if (r[i][j]==Max_NUM){
          r[i][j] = sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]))/v_p;
        }
      if (i==j) r[i][j]=Max_NUM;

    }

    vector<bool> Fixed; // Посещенность городов
    vector<double> Weights; // Итоговые расстояния
    vector<double> Put; // Итоговые расстояния

    for(size_t i=1;i<=n+3;++i){
      Weights.push_back(Max_NUM);
      Fixed.push_back(1);
    }

    int k, start = n+1;
    Weights[start]=0;
    while (true){

      double min = Max_NUM;
      size_t res;
      for(size_t i=1;i <= n+2;++i){
        if ((Weights[i] < min) && (Fixed[i])) {
          res = i;
          min = Weights[i];
        }
      }
      if (min == Max_NUM) k = -1; else {
        Fixed[res] = 0;
        k = res;
      }

      if (k==-1) break;

      for(int i=1; i<=n+2; ++i){
        if (r[k][i] + Weights[k] < Weights[i]){
          Weights[i] = r[k][i] + Weights[k];
        }
      }

    }

    if (Weights[k] == Max_NUM) cout<<"-1"; else cout<<setprecision(15)<<Weights[n+2]<<'\n';

      for(int i=1; i<=n+2; ++i) Fixed[i]=1;
      k = n+2; Fixed[k]=0;
      while (k != start) {
        for(int i=1; i<=n+2; ++i){
          if ((absol(Weights[k] - r[k][i] - Weights[i]) < 0.0000001) && (k!=i)/* && (Fixed[i])*/){
            Put.push_back(k); Fixed[i]=0;
            k = i;
            break;
          }
        }
      }
      Put.push_back(k);
      for(size_t i=0;i<Put.size()/2;++i){
        swap(Put[i],Put[Put.size()-i-1]);
      }
      cout<<Put.size()-2<<' ';
      for(size_t i=1;i<Put.size()-1;++i){
        cout<<Put[i]<<' ';
      }

}
