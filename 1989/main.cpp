#include <iostream>
#include <string>
#include <vector>
using namespace std;

const long long Q = 311, P = 1000*1000*100;

long long Q_st[100003], Q_OBR_st[100003];

long long modP(long long n) {
    return ((n % P) + P) % P;
}

int gcd (long long a, int b, int & x, int & y) {
        if (a == 0) {
            x = 0; y = 1;
            return b;
        }
        int x1, y1;
        int d = gcd (b%a, a, x1, y1);
        x = y1 - (b / a) * x1;
        y = x1;
        return d;
    }

    long long obr(long long q) {
        int x, y;
        int m = P;
        int g = gcd (q, m, x, y);
        if (g != 1)
            cout << "no solution";
        else {
            x = (x % m + m) % m;
            return x;
        }
    }

const long long Q_OBR = obr(Q);

long long my_pow(long long q, int k) {
    if (q == Q_OBR) {
        return Q_OBR_st[k];
    } else {
        return Q_st[k];
    }
}

class intervalTree {
    vector<long long> tree;
    int sdvig;
    int len;

public:

    intervalTree(const string &s) {
        int n = s.length();
        len = n;
        int kol2 = stepen2(n);
        int st2 = 1<<kol2;
        sdvig = st2-1;

        tree.resize(st2 * 2 - 1);
        for (int i=0;i<s.length();++i) {
            tree[i + sdvig] = modP((int)(s[i]) * my_pow(Q, n-i-1));
        }
        for (int i=sdvig-1;i>=0;--i) {
            tree[i] = modP(tree[2*i+1] + tree[2*i+2]);
        }
    }

    int size() {
        return len;
    }



    void change(int num, long long value) {
        num += sdvig;
        long long delta = modP(value - tree[num]);
        while (true) {
            tree[num] = modP(tree[num] + delta);
            if (num == 0) {
                break;
            }
            num = (num-1)/2;
        }
    }

    long long sum(int l, int r) { // sum[l .. r]
        if (r<l) {
            return 0;
        }
        l += sdvig;
        r += sdvig;
        long long res=0;
        while (l<=r){
            if (l%2==0) { res+=tree[l]; l=l/2; } else l=(l-1)/2;
            if (r%2==1) { res+=tree[r]; r=(r-3)/2; } else r=(r-2)/2;
        }
        return res;
    }

    int stepen2(int n) {
        int st = 1, kol = 0;
        while (st < n) {
            st *= 2;
            ++kol;
        }
        return (kol);
    }

    long long hash(int l, int r) { // hash[l .. r-1]
        return modP(modP(sum(0, r-1) - sum(0, l-1))*my_pow(Q_OBR, len-r));
    }

    void print() {
        int t = 2;
        for (int i=0;i<tree.size();++i) {
            cout<<tree[i]<<" ";
            if (i == t-2) {
                cout<<"\n";
                t*=2;
            }
        }
    }

};

void change(int ind, char c, intervalTree &t1, intervalTree &t2) {
    --ind;
    int n = t1.size();
    t1.change(ind, modP((int)(c)*my_pow(Q, n-ind-1)));
    t2.change(n-ind-1, modP((int)(c)*my_pow(Q, ind)));
}

bool polind(int l, int r, intervalTree &t1, intervalTree &t2, const string &s) {
    --l; --r;
    int n = t1.size();
    if (t1.hash(l, r+1) == t2.hash(n - r - 1, n - l)) {
        return true;
    } else {
        return false;
    }
}

int main() {
    //freopen("1.in", "r", stdin);
    int t=1;
    for(int i=0;i<=100002;++i) {
        Q_st[i]=t;
        t = (t*Q) % P;
    }
    t=1;
    for(int i=0;i<=100002;++i) {
        Q_OBR_st[i]=t;
        t = (t*Q_OBR) % P;
    }
    string s1, s;
    int n,l,r;
    char c;
    cout.sync_with_stdio(false);
    cin.sync_with_stdio(false);

    cin>>s;
    cin>>n;

    intervalTree tree1(s), tree2(string(s.rbegin(), s.rend()));

    for (int i=0;i<n;++i) {
        cin>>s1;
        if (s1[0] == 'p') {
            cin>>l>>r;
            if (polind(l, r, tree1, tree2, s)) {
                cout << "Yes\n";
            } else {
                cout << "No\n";
            }
        } else {
            cin >> l >> c;
            change(l, c, tree1, tree2);
            s[l-1] = c;
        }
    }
    return 0;
}
