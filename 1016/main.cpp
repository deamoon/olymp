#include <iostream>
#include <vector>
//#include <math.h>
using namespace std;

double absol(double a){
  if (a>=0) return a; else return -a;
}

double kub[7];

struct kletka{
    int i, j, z;
};

double down_kl(int z){
    return kub[z/4];
}

int right(int z){
    if (z==0) return 22;  if (z==1) return 19;  if (z==2) return 10;  if (z==3) return 15;
    if (z==4) return 18;  if (z==5) return 23;  if (z==6) return 14;  if (z==7) return 11;
    if (z==8) return 16;  if (z==9) return 21;  if (z==10) return 6; if (z==11) return 3;
    if (z==12) return 20; if (z==13) return 17; if (z==14) return 2; if (z==15) return 7;
    if (z==16) return 12; if (z==17) return 9; if (z==18) return 0; if (z==19) return 5;
    if (z==20) return 8; if (z==21) return 13; if (z==22) return 4; if (z==23) return 1;
}
int left(int z){
    if (z==0) return 18;  if (z==1) return 23;  if (z==2) return 14;  if (z==3) return 11;
    if (z==4) return 22;  if (z==5) return 19;  if (z==6) return 10;  if (z==7) return 15;
    if (z==8) return 20;  if (z==9) return 17;  if (z==10) return 2; if (z==11) return 7;
    if (z==12) return 16; if (z==13) return 21; if (z==14) return 6; if (z==15) return 3;
    if (z==16) return 8; if (z==17) return 13; if (z==18) return 4; if (z==19) return 1;
    if (z==20) return 12; if (z==21) return 9; if (z==22) return 0; if (z==23) return 5;
}
int vpered(int z){
    if (z==0) return 12;  if (z==1) return 8;  if (z==2) return 20;  if (z==3) return 16;
    if (z==4) return 13;  if (z==5) return 9;  if (z==6) return 21;  if (z==7) return 17;
    if (z==8) return 4;  if (z==9) return 0;  if (z==10) return 22; if (z==11) return 18;
    if (z==12) return 5; if (z==13) return 1; if (z==14) return 23; if (z==15) return 19;
    if (z==16) return 6; if (z==17) return 2; if (z==18) return 14; if (z==19) return 10;
    if (z==20) return 7; if (z==21) return 3; if (z==22) return 15; if (z==23) return 11;
}
int nazad(int z){
    if (z==0) return 9;  if (z==1) return 13;  if (z==2) return 17;  if (z==3) return 21;
    if (z==4) return 8;  if (z==5) return 12;  if (z==6) return 16;  if (z==7) return 20;
    if (z==8) return 1;  if (z==9) return 5;  if (z==10) return 19; if (z==11) return 23;
    if (z==12) return 0; if (z==13) return 4; if (z==14) return 18; if (z==15) return 22;
    if (z==16) return 3; if (z==17) return 7; if (z==18) return 11; if (z==19) return 15;
    if (z==20) return 2; if (z==21) return 6; if (z==22) return 10; if (z==23) return 14;
}

int main() {
    //freopen("1.in","r",stdin);  //freopen("output.txt","w",stdout);
    //int r[10][10][7][7];
    double Max_NUM = 2*1000*1000*1000;
    bool Fixed[10][10][24]; // Посещенность городов
    double Weights[10][10][24]; // Итоговые расстояния
    //int sos[10][10]; // Кол-во состояний
    vector<kletka> Put; // Итоговый путь
    kletka start, finish;
    char c;

    cin>>c; start.i = (int) c - (int) 'a' + 1;
    cin>>c; start.j = (int) c - (int) '1' + 1;
    cin>>c; finish.i = (int) c - (int) 'a' + 1;
    cin>>c; finish.j = (int) c - (int) '1' + 1;

    for (int i=0; i<6; ++i) {  cin>>kub[i];
    //    if ((kub[i]==0) && (kub[1]%2!=0)) while (1) {Put.push_back(start);}
      //if (absol(kub[i])<=0.00000001) { kub[i] = kub[i] + t*0.0001; t++; }
    }
    swap(kub[2],kub[5]);

    //cout<<start.i<<' '<<start.j<<' '<<finish.i<<' '<<finish.j<<'\n';
    //for (int i=1; i<=6; ++i) cout<<kub[i]<<' ';






////////////////////////////////////////////////////////////////


    for(int i=0; i<=9; ++i)
        for(int j=0; j<=9; ++j)
            for(int z=0; z<24; ++z) {
                Weights[i][j][z] = Max_NUM;
                Fixed[i][j][z] =  1;
            }

    kletka k; start.z = 16;
    Weights[start.i][start.j][16] = kub[4];
    while (true) {
        double min = Max_NUM;
        kletka res;
        for(int i=1; i<=8; ++i)
            for(int j=1; j<=8; ++j)
                for(int z=0; z<24; ++z)
                    if ((Weights[i][j][z] < min) && (Fixed[i][j][z])) {
                        res.i = i; res.j = j; res.z = z;
                        min = Weights[i][j][z];
                    }
        if (min == Max_NUM) break; else {
            Fixed[res.i][res.j][res.z] = 0;
            k = res;
        }

        if (down_kl(right(k.z)) + Weights[k.i][k.j][k.z] < Weights[k.i+1][k.j][right(k.z)]){ // вправо
            Weights[k.i+1][k.j][right(k.z)] = down_kl(right(k.z)) + Weights[k.i][k.j][k.z];
        }
        if (down_kl(left(k.z)) + Weights[k.i][k.j][k.z] < Weights[k.i-1][k.j][left(k.z)]){ // влево
            Weights[k.i-1][k.j][left(k.z)] = down_kl(left(k.z)) + Weights[k.i][k.j][k.z];
        }
        if (down_kl(vpered(k.z)) + Weights[k.i][k.j][k.z] < Weights[k.i][k.j+1][vpered(k.z)]){ // вперед
            Weights[k.i][k.j+1][vpered(k.z)] = down_kl(vpered(k.z)) + Weights[k.i][k.j][k.z];
        }
        if (down_kl(nazad(k.z)) + Weights[k.i][k.j][k.z] < Weights[k.i][k.j-1][nazad(k.z)]){ // назад
            Weights[k.i][k.j-1][nazad(k.z)] = down_kl(nazad(k.z)) + Weights[k.i][k.j][k.z];
        }

        /*for(int j=8; j>=1; --j) {
            for(int i=1; i<=8; ++i)
                if (Weights[i][j]<0) exit(1); else
                    if (Weights[i][j]==Max_NUM) cout<<"0 "; else cout<<Weights[i][j]<<' ';
            cout<<'\n';
        }
        cout<<"\n\n";*/
        //cout<<Weights[finish.i][finish.j]<<'\n';
    }

    double min = Max_NUM;
    for(int i=0; i<24; ++i) {
        if (Weights[finish.i][finish.j][i]<min) {
            min = Weights[finish.i][finish.j][i];
            finish.z = i;
        }
    }
    cout<<Weights[finish.i][finish.j][finish.z]<<' ';

    int t=1;
    for (int i=0; i<6; ++i) {
      //if (absol(kub[i])<=0.000001) { kub[i] = kub[i] + t*0.0001; t++; }
      kub[i] = kub[i] + t*0.0001; t++;
    }

//////////////////////////////////////////////////////////////////



    for(int i=0; i<=9; ++i)
        for(int j=0; j<=9; ++j)
            for(int z=0; z<24; ++z) {
                Weights[i][j][z] = Max_NUM;
                Fixed[i][j][z] =  1;
            }

    //kletka k;
    start.z = 16;
    Weights[start.i][start.j][16] = kub[4];
    while (true) {
        double min = Max_NUM;
        kletka res;
        for(int i=1; i<=8; ++i)
            for(int j=1; j<=8; ++j)
                for(int z=0; z<24; ++z)
                    if ((Weights[i][j][z] < min) && (Fixed[i][j][z])) {
                        res.i = i; res.j = j; res.z = z;
                        min = Weights[i][j][z];
                    }
        if (min == Max_NUM) break; else {
            Fixed[res.i][res.j][res.z] = 0;
            k = res;
        }

        if (down_kl(right(k.z)) + Weights[k.i][k.j][k.z] < Weights[k.i+1][k.j][right(k.z)]){ // вправо
            Weights[k.i+1][k.j][right(k.z)] = down_kl(right(k.z)) + Weights[k.i][k.j][k.z];
        }
        if (down_kl(left(k.z)) + Weights[k.i][k.j][k.z] < Weights[k.i-1][k.j][left(k.z)]){ // влево
            Weights[k.i-1][k.j][left(k.z)] = down_kl(left(k.z)) + Weights[k.i][k.j][k.z];
        }
        if (down_kl(vpered(k.z)) + Weights[k.i][k.j][k.z] < Weights[k.i][k.j+1][vpered(k.z)]){ // вперед
            Weights[k.i][k.j+1][vpered(k.z)] = down_kl(vpered(k.z)) + Weights[k.i][k.j][k.z];
        }
        if (down_kl(nazad(k.z)) + Weights[k.i][k.j][k.z] < Weights[k.i][k.j-1][nazad(k.z)]){ // назад
            Weights[k.i][k.j-1][nazad(k.z)] = down_kl(nazad(k.z)) + Weights[k.i][k.j][k.z];
        }

        /*for(int j=8; j>=1; --j) {
            for(int i=1; i<=8; ++i)
                if (Weights[i][j]<0) exit(1); else
                    if (Weights[i][j]==Max_NUM) cout<<"0 "; else cout<<Weights[i][j]<<' ';
            cout<<'\n';
        }
        cout<<"\n\n";*/
        //cout<<Weights[finish.i][finish.j]<<'\n';
    }

    min = Max_NUM;
    for(int i=0; i<24; ++i) {
        if (Weights[finish.i][finish.j][i]<min) {
            min = Weights[finish.i][finish.j][i];
            finish.z = i;
        }
    }
    //cout<<Weights[finish.i][finish.j][finish.z]<<' ';
    for(int i=0; i<24; ++i) {
        if (Weights[finish.i][finish.j][i]==min) {
            //cout<<i<<'\n';
        }
    }
    //finish.z = 12;

    for(int i=0; i<=9; ++i)
        for(int j=0; j<=9; ++j)
            for(int z=0; z<24; ++z) {
                Fixed[i][j][z] =  1;
                if ( (i*j == 0) || (i==9) || (j==9) ) Weights[i][j][z] =  Max_NUM;
            }
    //cout<<Weights[finish.i][finish.j][finish.z]<<' '<<Weights[start.i][start.j][start.z]<<'\n';
    //exit(0);
    //for(int i=1; i<=n+2; ++i) Fixed[i]=1;
    //int w = 0;
    double Min_NUM = 0.00000001;
    k = finish; Fixed[finish.i][finish.j][finish.z] = 0;
    while ( (k.i != start.i) || (k.j != start.j) || (k.z != start.z) ) {
        //++w;
        if (( absol(Weights[k.i][k.j][k.z] - down_kl(k.z) - Weights[k.i+1][k.j][right(k.z)])<Min_NUM ) && (Fixed[k.i+1][k.j][right(k.z)])) {
            Put.push_back(k);
            Fixed[k.i+1][k.j][right(k.z)] = 0;
            k.i = k.i+1; k.z = right(k.z);
        } else
        if (( absol(Weights[k.i][k.j][k.z] - down_kl(k.z) - Weights[k.i-1][k.j][left(k.z)])<Min_NUM ) && (Fixed[k.i-1][k.j][left(k.z)])) {
            Put.push_back(k);
            Fixed[k.i-1][k.j][left(k.z)] = 0;
            k.i = k.i-1; k.z = left(k.z);
        } else
        if (( absol(Weights[k.i][k.j][k.z] - down_kl(k.z) - Weights[k.i][k.j+1][vpered(k.z)])<Min_NUM ) && (Fixed[k.i][k.j+1][vpered(k.z)])){
            Put.push_back(k);
            Fixed[k.i][k.j+1][vpered(k.z)] = 0;
            k.j = k.j+1; k.z = vpered(k.z);
        } else
        if (( absol(Weights[k.i][k.j][k.z] - down_kl(k.z) - Weights[k.i][k.j-1][nazad(k.z)])<Min_NUM ) && (Fixed[k.i][k.j-1][nazad(k.z)])) {
            Put.push_back(k);
            Fixed[k.i][k.j-1][nazad(k.z)] = 0;
            k.j = k.j-1; k.z = nazad(k.z);
            //cout<<' '<<k.i<<' '<<k.j<<' '<<k.z;
            //cout<<'\n'<<start.i<<' '<<start.j<<' '<<start.z; exit(0);
        } else { cout<<'\n'<<"wqe\n"; break; }
        //if (w>=100) break;
    }

    Put.push_back(k);
    for(size_t i=0; i<Put.size()/2; ++i){
        swap(Put[i],Put[Put.size()-i-1]);
    }
    //cout<<Put.size()-2<<' ';
    for(size_t i=0; i<Put.size(); ++i){
        cout<<(char) (Put[i].i + (int) 'a' - 1)<<Put[i].j<<' ';
    }

}
