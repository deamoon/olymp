#include <iostream>
#include <fstream>
using namespace std;
const int nlist=32768, RAZ=65536, GG=1000*1000*1000;
int a[RAZ];

void update(int x, int k){
    int t = x+nlist;
    int d = k-a[t];
    while (t>=1) {
      a[t]=(a[t]+d)%GG; t=t/2;
    }
}

int RSQ(int l, int r){
    if (l>r) return 0;
    l+=nlist; r+=nlist; long long int res=0;
    while (l<=r){
      if (l%2==1) { res=(res+a[l])%GG; l=(l+1)/2; } else l=l/2;
      if (r%2==0) { res=(res+a[r])%GG; r=(r-1)/2; } else r=r/2;
    }
    int res1=res%GG;
    return res1;
}

int main()
{
    //freopen("in","r",stdin);
    //freopen("out","w",stdout);
    for (int i=0;i<RAZ;++i) a[i]=0;

    int x,n,k,q;
    cin>>n>>k;
    int b[20001], c[20001];
    for(int i=1;i<=n;++i) {cin>>b[i]; c[i]=1;}

    for(int i=2;i<=k;++i){
      for(int j=1;j<=n;++j) {
        x=b[j]; q=c[x];
        c[x]=RSQ(x+1,n);
        update(x,q);
      }
      for (int i=0;i<RAZ;++i) a[i]=0;
    }
    long long int res=0;
    for(int i=1;i<=n;++i) res=(res+c[i])%GG;
    cout<<res;
    return 0;
}
