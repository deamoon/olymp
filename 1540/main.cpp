#include <iostream>
#include <vector>
#include <map>
#include <stdlib.h>

using namespace std;

int const D = 100;
bool used[D+1];
vector<int> w1;
vector< vector<int> > w2;

const long long Q = 311, P = 1000*1000*100;
map<long long, int> cache;

long long modP(long long n) {
    return ((n % P) + P) % P;
}

long long mhash(const vector<int> &a) {
    int h = 0;
    for (int i = 0; i < a.size(); ++i) {
        h = modP(h*Q + a[i]);
    }
    return h;
}

int nim(const vector< vector<int> > &a, int num);

int mex(const vector<int> &a) {
	int c = (int) a.size();

	for (int i=0; i<c; ++i) {
        used[a[i]] = true;
	}

	int result;
	for (int i=0; ; ++i) {
		if (!used[i]) {
			result = i;
            break;
		}
	}

	for (int i=0; i<c; ++i) {
        used[a[i]] = false;
	}

	return result;
}

vector< vector<int> > split(const vector<int> &a, int x) {
    vector< vector<int> > b;
    bool q = true;

    for (int i = 0; i < a.size(); ++i) {
        if (q) {
            if (a[i] > x) {
                b.push_back(vector<int>());
                b[b.size()-1].push_back(a[i]);
                q = false;
            }
        } else {
            if (a[i] > x) {
                b[b.size()-1].push_back(a[i]);
            } else {
                q = true;
            }
        }
    }

    return b;
}

int grandi(const vector<int> &a, int num, int cep) {
    vector<int> b;
    for (int i = 0; i < a.size(); ++i) {
        int ni = nim(split(a, a[i]), num + 1);

        if (num == 1) {
            w2[cep].push_back(ni);
        }

        b.push_back(ni);
    }
    return mex(b);
}

int nim(const vector< vector<int> > &a, int num) {
    if (a.size() == 0) {
        return 0;
    }

    int res;
    long long ha = mhash(a[0]);
    map<long long, int>::iterator it = cache.find(ha);
    if (it != cache.end()) {
        res = it->second;
    } else {
        res = grandi(a[0], num, 0);
        cache[ha] = res;
    }

    if (num == 1) {
        w1.push_back(res);
    }

    for (int i = 1; i < a.size(); ++i) {
        int g;

        ha = mhash(a[i]);
        it = cache.find(ha);
        if (it != cache.end()) {
            g = it->second;
        } else {
            g = grandi(a[i], num, i);
            cache[ha] = g;
        }

        if (num == 1) {
            w1.push_back(g);
        }

        res ^= g;
    }
    return res;
}

int main() {
    //freopen("1.in", "r", stdin);

    int n, k ,p, old;
    cin>>n;
    vector< vector<int> > a;

    for (int i = 0; i < n; ++i) {
        cin >> k;
        a.push_back(vector<int>());
        w2.push_back(vector<int>());
        old = -1;
        for (int j = 0; j < k; ++j) {
            cin >> p;
            //if (p != old) {
                a[i].push_back(p);
            //}
            //old = p;
        }
    }

    for (int i=0; i<=D; ++i) {
        used[i] = false;
	}

    if (nim(a, 1) == 0) {
        cout << "S";
    } else {
        cout << "G\n";


        for (int i = 0; i < w2.size(); ++i) {
            for (int j = 0; j < w2[i].size(); ++j) {
                int sum = 0;
                for (int z = 0; z < w1.size(); ++z) {
                    if (z == i) {
                        sum ^= w2[i][j];
                    } else {
                        sum ^= w1[z];
                    }
                }
                if (sum == 0) {
                    cout << i+1 << " " << j+1;
                    exit(0);
                }
            }
        }

    }

    //for (int i = 0; i < a.size(); ++i) {
    //    for (int j = 0; j < a[i].size(); ++j) {
    //        cout<<a[i][j]<<" ";
    //    }
    //    cout<<endl;
    //}

    return 0;
}
