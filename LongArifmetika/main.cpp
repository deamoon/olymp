#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

vector<short int> sum(vector<short int> &a, vector<short int> &b){
  vector<short int> c;
  if (a.size()>b.size()) {
    for(size_t i=b.size();i<a.size();++i) b.push_back(0);
  } else {
    for(size_t i=a.size();i<b.size();++i) a.push_back(0);
  }
  int p=0;
  for(size_t i=0;i<max(a.size(),b.size());++i) {
    c.push_back((a[i]+b[i]+p)%10);
    p=(a[i]+b[i]+p)/10;
  }
  if (p!=0) c.push_back(p);
  return c;
}

vector<short int> prInt(vector<short int> &a, int b){
  vector<short int> c;
  int p=0;
  for(size_t i=0;i<a.size();++i) {
    c.push_back((a[i]*b+p)%10);
    p=(a[i]*b+p)/10;
  }
  if (p!=0) c.push_back(p);
  return c;
}

vector<short int> pr(vector<short int> &a, vector<short int> &b){
  vector<short int> c,d;
  c.push_back(0);
  for(size_t i=0;i<b.size();++i) {
    d=prInt(a,b[i]);
    for(size_t k=0;k<i;++k) d.push_back(0);
    for(int k=d.size()-1-i;k>=0;--k) d[k+i]=d[k];
    for(size_t k=0;k<i;++k) d[k]=0;
    c=sum(c,d);
  }
  return c;
}

vector<short int> StrToInt(string &s){
  vector<short int> a;
  for(int i=s.length()-1;i>=0;--i){
    a.push_back(int(s[i])-int('0'));
  }
  return a;
}

ifstream f1; ofstream f2;
void write(vector<short int> &a){
    for(int i=a.size()-1;i>=0;--i) f2<<a[i];
}

int main()
{
  vector<short int> a,b,c;
  string s1,s2;  f1.open("input.txt"); f2.open("output.txt");
  getline(f1,s1);getline(f1,s2);
  a=StrToInt(s1); b=StrToInt(s2);
  c=pr(a,b);
  write(c);
  f1.close(); f2.close();
  return 0;
}
