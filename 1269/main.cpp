#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <climits>

using namespace std;

class Bor {

    typedef unordered_map<int, int> vertexCharMap;
    typedef vertexCharMap::const_iterator borIterator;

    struct Vertex {
        int endLink;
        int pred;
        int link;
        int distRoot;
    };

    vertexCharMap mapBor;
    vector<Vertex> tBor;

    const static int INIT_LINK = -1, NOT_END_LINK = -2;

    public:

        Bor() {
            tBor.push_back(Vertex());
            tBor[0].link = 0;
            tBor[0].endLink = INIT_LINK;
            tBor[0].distRoot = 0;
        }

        void buildFromStdIn(int countSample) {
            string sample;
            for (int index = 0; index <= countSample; ++index) {
                getline(cin, sample);
                if (sample.length() > 0) {
                    addSample(sample);
                }
            }
        }

        void findInStr(const string &textString, int indexLine, pair<int, int> &answerIndexLinePosChar) {
            int vertex = 0;
            for (int indexString=0; indexString < textString.length(); ++indexString) {
                int currentChar = (unsigned char) textString[indexString];

                while (true) {
                    int vertexChar = doPair(vertex, currentChar);
                    int link = getLink(vertex);
                    borIterator iteratorVertexChar = mapBor.find(vertexChar);
                    if (iteratorVertexChar != mapBor.end()) {
                        vertex = iteratorVertexChar->second;
                        int endLink = getEndLink(vertex);
                        if (endLink >= 0) {
                            int posChar = indexString + 2 - tBor[endLink].distRoot;
                            reCalcAnswer(answerIndexLinePosChar, indexLine, posChar);
                        }
                        break;
                    }
                    if (vertex == 0) {
                        break;
                    }
                    vertex = link;
                }

            }
        }

    private:

        int doPair(int first, int second) {
            int pair;
            pair = first;
            pair = pair << 8;
            pair += second;
            return pair;
        }

        int getVertex(int pair) {
            return (int)(pair >> 8);
        }

        int getChar(int t) {
            return (int)((t << 24) >> 24);
        }

        void reCalcAnswer(pair<int, int> &answerIndexLinePosChar, int indexLine, int posChar) {
            if (indexLine <= answerIndexLinePosChar.first && posChar < answerIndexLinePosChar.second) {
                answerIndexLinePosChar.first = indexLine;
                answerIndexLinePosChar.second = posChar;
            }
        }

        void addSample (const string & sample) {
            int currentVertex = 0;
            for (int indexString = 0; indexString < sample.length(); ++indexString) {
                unsigned char currentChar = (unsigned char) sample[indexString];
                int vertexChar = doPair(currentVertex, currentChar);
                borIterator iteratorVertexChar = mapBor.find(vertexChar);
                if (iteratorVertexChar == mapBor.end()) {
                    int newVertex = tBor.size();
                    mapBor[vertexChar] = newVertex;
                    tBor.push_back(Vertex());

                    tBor[newVertex].link = INIT_LINK;
                    tBor[newVertex].endLink = INIT_LINK;
                    tBor[newVertex].pred = vertexChar;
                    tBor[newVertex].distRoot = indexString + 1;

                    currentVertex = newVertex;
                } else {
                    currentVertex = iteratorVertexChar->second;
                }
            }
            tBor[currentVertex].endLink = currentVertex;
        }

        int getLink(int vertex) {
            if (tBor[vertex].link == INIT_LINK) {
                if (vertex == 0 || getVertex(tBor[vertex].pred) == 0) {
                    tBor[vertex].link = 0;
                } else {
                    int predVertexIndex = getVertex(tBor[vertex].pred);
                    int predChar = getChar(tBor[vertex].pred);
                    while (true) {
                        predVertexIndex = getLink(predVertexIndex);
                        int predVertexChar = doPair(predVertexIndex, predChar);
                        borIterator iteratorPredVertexChar = mapBor.find(predVertexChar);
                        if (iteratorPredVertexChar != mapBor.end()) {
                            tBor[vertex].link = iteratorPredVertexChar->second;
                            break;
                        }
                        if (predVertexIndex == 0) {
                            tBor[vertex].link = 0;
                            break;
                        }
                    }
                }
            }
            return tBor[vertex].link;
        }

        int getEndLink(int vertex) {
            if (tBor[vertex].endLink == INIT_LINK) {
                if (vertex == 0 || getVertex(tBor[vertex].pred) == 0) {
                    tBor[vertex].endLink = NOT_END_LINK;
                } else {
                    tBor[vertex].endLink = getEndLink(getLink(vertex));
                }
            }
            return tBor[vertex].endLink;
        }
};

pair<int, int> calcAnswerFromStdIn(Bor &bor, int countTextString) {
    pair<int, int> answerIndexLinePosChar(LONG_MAX, LONG_MAX);
    string textString;
    for (int indexLine = 0; indexLine <= countTextString; ++indexLine) {
        getline(cin, textString);
        if (textString.length() <= 0) {
            continue;
        }

        bor.findInStr(textString, indexLine, answerIndexLinePosChar);

    }
    return answerIndexLinePosChar;
}

void printAnswer(pair<int, int> lineChar) {
    if (lineChar.second != LONG_MAX) {
        cout << lineChar.first << " " << lineChar.second;
    } else {
        cout << "Passed";
    }
}

int readInputInt(int &count) {
    cin >> count;
}

int main() {
    Bor bor;
    int countSample;
    int countTextString;

    //freopen("1.in", "r", stdin);
    readInputInt(countSample);
    bor.buildFromStdIn(countSample);

    readInputInt(countTextString);
    printAnswer(calcAnswerFromStdIn(bor, countTextString));

    return 0;
}
