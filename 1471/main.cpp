#include <iostream>
#include <vector>
#include <limits>
#include <math.h>
#pragma comment(linker, "/STACK:1000000000")
using namespace std;
typedef int(*TComparators)(const int &a, const int &b);

template  <typename T>
T min(const T &a, const T &b){if (a<b) return a; else return b;}

template  <typename T>
struct TTraits{
  TComparators f;
  T Zero;
  TTraits(TComparators f1, T z){
    f=f1; Zero=z;
  }
};
template  <typename T>
class TIntervalTree{
  private:
    T *tree;
    size_t size;
    TComparators cmp;
    T Zero;
    int *num;
  public:
    template <typename S>
    TIntervalTree(size_t n, vector<T> &a, S mono){ //Конструктор, принимающий массив
      size=1; cmp=mono.f; Zero=mono.Zero;
      while (size<n) size*=2;
      tree = new T[2*size-1]; num = new T[2*size-1];
      for(size_t i=0;i<n;++i) {tree[i+size-1]=a[i]; num[i+size-1]=i;}
      for(size_t i=n;i<size;++i) {tree[i+size-1] = Zero; num[i+size-1] = -1;}
      for(int i=size-2;i>=0;--i) {
        tree[i]=cmp(tree[2*i+1],tree[2*i+2]);
        if (tree[2*i+1]<tree[2*i+2]) num[i]=num[2*i+1]; else num[i]=num[2*i+2];
      }
    }
    int calc(int l, int r){
      l+=size-1; r+=size-1;
      int res = Zero; int otv;
      while (l<=r){
        if (l%2==0) { if (tree[l]<res) otv=l; res=cmp(res,tree[l]); l=l/2; } else l=l/2;
        if (r%2==1) { if (tree[r]<res) otv=r; res=cmp(res,tree[r]); if (r!=1) r=(r-2)/2; else r=-1; } else r=(r-2)/2;
      }
      return num[otv];
    }
    ~TIntervalTree(){
      delete[] tree;
    }
};

struct ver{
  int num;
  int value;
  ver(){};
  ver(int n, int v){
    num=n; value=v;
  }
};

vector<vector<ver> > tree; // Дерево задано списком смежности
vector<int> val; // Расстояние от вершины до корня (номер 0)

vector<int> E; // Вершина в порядке обхода DFS
vector<int> L; // Уровни вершин в порядке обхода DFS
vector<int> H; // Номер в E, первой встечи вершины
vector<int> used; // Для DFS
size_t n;

void tree_value(int v,int k){
  used[v]=0;
  for(size_t i=0;i<tree[v].size();++i){
    if (used[tree[v][i].num]) {
      val[tree[v][i].num] = val[v] + tree[v][i].value;
      E.push_back(v); L.push_back(k); if (H[v]==-1) H[v]=E.size()-1;
      tree_value(tree[v][i].num,k+1);
    }
  }
  E.push_back(v); L.push_back(k); if (H[v]==-1) H[v]=E.size()-1;
}

void input(){
  int a,b,v;
  cin>>n; tree.resize(n);
  for(size_t i=1;i<=n-1;++i){
    cin>>a>>b>>v;
    tree[a].push_back(ver(b,v));
    tree[b].push_back(ver(a,v));
  }
}

int main()
{
    freopen("in","r",stdin);
    freopen("out","w",stdout);

    input();

    val.resize(n); val[0]=0; used.assign(n,1); H.assign(n,-1);
    tree_value(0,0);

    TTraits<int> Min(&min,numeric_limits<int>::max());
    TIntervalTree<int> InterTree(L.size(),L,Min);

    int m,a,b;
    cin>>m;
    for(int i=0;i<m;i++){
      cin >> a >> b;
      int h1=H[a], h2=H[b]; if (h2<h1) swap(h1,h2);
      int lca = E[InterTree.calc(h1,h2)];
      cout<<val[a]+val[b]-2*val[lca]<<'\n';
    }

    return 0;
}
