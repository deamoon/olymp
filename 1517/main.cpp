#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <vector>
using namespace std;

// константы
const int maxlen = 100000*2+10;
const int alphabet = 256;

int pn[maxlen], cn[maxlen];
int p[maxlen], cnt[maxlen], c[maxlen];
int classes;

void print(int n) {
    cout<<"\n";
    for (int i=0;i<n;++i) {
        cout<<p[i]<<" ";
    }
    cout<<"\n";
}

void buildSufMas(vector<char> &s) {
    int n = s.size();
    memset (cnt, 0, alphabet * sizeof(int));
    for (int i=0; i<n; ++i)
        ++cnt[s[i]];
    for (int i=1; i<alphabet; ++i)
        cnt[i] += cnt[i-1];
    for (int i=0; i<n; ++i)
        p[--cnt[s[i]]] = i;
    c[p[0]] = 0;
    classes = 1;
    for (int i=1; i<n; ++i) {
        if (s[p[i]] != s[p[i-1]])  ++classes;
        c[p[i]] = classes-1;
    }

    //print(s.size());

    for (int h=0; (1<<h)<n; ++h) {
        for (int i=0; i<n; ++i) {
            pn[i] = p[i] - (1<<h);
            if (pn[i] < 0)  pn[i] += n;
        }
        memset (cnt, 0, classes * sizeof(int));
        for (int i=0; i<n; ++i)
            ++cnt[c[pn[i]]];
        for (int i=1; i<classes; ++i)
            cnt[i] += cnt[i-1];
        for (int i=n-1; i>=0; --i)
            p[--cnt[c[pn[i]]]] = pn[i];
        cn[p[0]] = 0;
        classes = 1;
        for (int i=1; i<n; ++i) {
            int mid1 = (p[i] + (1<<h)) % n,  mid2 = (p[i-1] + (1<<h)) % n;
            if (c[p[i]] != c[p[i-1]] || c[mid1] != c[mid2])
                ++classes;
            cn[p[i]] = classes-1;
        }
        memcpy (c, cn, n * sizeof(int));

        //print(s.size());
    }
}

vector<int> solve(vector<char> &s, int *pos) {
    vector<int> rank(s.size());
    for (int i = 0; i < s.size(); i++) {
        rank[pos[i]] = i;
    }

    /*for (int i=0;i<s.size();++i) {
        cout<<rank[i]<<' ';
    }
    cout<<"\n";*/

    vector<int> ans(s.size());

    int h = 0;
    for (int i = 0; i < s.size(); i++) {
        if (rank[i] == s.size()-1) {
            continue;
        }
        int k = pos[rank[i] + 1];
        //cout<<i<<" "<<k<<"\n";
        while (((i + h) != s.size()) && ((k + h) != s.size()) && (s[i + h] == s[k + h])) {
            h++;
        }
        ans[rank[i]] = h;
        if (h > 0) {
            h--;
        }

    }

    return ans;
}

int main() {
    int n;
    //freopen("1.in", "r", stdin);
    cin>>n;
    vector<char> s(2*n+2);
    for (int i=0;i<n;++i) {
        cin>>s[i];
    }
    s[n] = '$';
    for (int i=n+1;i<=2*n;++i) {
        cin>>s[i];
    }
    s[2*n+1] = '#';
    /*for (int i=0;i<s.size();++i) {
        cout<<s[i];
    }*/


    buildSufMas(s);


    vector<int> ans = solve(s, p);

    //cout<<"\n";
    int max = -1;
    int it = 0;
    //int n = s.size();
    for (int i=0;i<s.size()-1;++i) {
        if ((p[i] < n && p[i+1] > n) || (p[i] > n && p[i+1] < n)) {
            if (ans[i] > max) {
                max = ans[i];
                it = p[i];
            }
        }
    }

    for (int i=it;i<it+max;++i) {
        cout<<s[i];
    }

    return 0;
}
