#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;

vector< vector<int> > list;
vector< int > value;
vector< int > pos;

void DFS(int k, int odd) {
    for(int i=0;i<list[k].size();++i) {
        if (!pos[list[k][i]]) {
            pos[list[k][i]] = 1;
            DFS(list[k][i], 1-odd);
        }
    }
    int a = 0, b = 0;
    for(int i=0;i<list[k].size();++i) {
        if (pos[list[k][i]] < 2) continue;

        if (value[list[k][i]] == 0) {
            ++a;
        } else {
            ++b;
        }
    }
    if (odd == 0) {
        if (a > 0) {
            value[k] = 0;
        } else {
            value[k] = 1;
        }
    } else {
        if (b > 0) {
            value[k] = 1;
        } else {
            value[k] = 0;
        }
    }
    pos[k] = 2;
}

int main() {
    //freopen("1.in", "r", stdin);
    int n, k, a, b;
    cin>>n>>k;
    list.resize(n+1);
    value.resize(n+1);
    pos.resize(n+1);

    for (int i=0;i<n-1;++i) {
        cin>>a>>b;
        list[a].push_back(b);
        list[b].push_back(a);
    }

    pos[k] = 1;
    DFS(k, 0);
    int min = n+2;
    for(int i=0;i<list[k].size();++i) {
        if (value[list[k][i]] == 0 && list[k][i] < min) {
            min = list[k][i];
        }
    }
    if (min == n+2) {
        printf("First player loses");
    } else {
        printf("First player wins flying to airport %d", min);
    }

    return 0;
}
