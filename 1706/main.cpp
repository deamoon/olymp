#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <vector>

using namespace std;

class SufMas {
    const static int MAX_LEN = 4005;
    const static int ALPHABET_SIZE = 256;
    int sortPair[MAX_LEN], newOrdClass[MAX_LEN];
    int sufMas[MAX_LEN], numberElement[MAX_LEN], ordClass[MAX_LEN];
    int classes;
    int lenStr;
    string str;

    public:

        SufMas(const string &s) {
            classes = 0;
            lenStr = s.length();
            str = s;
        }

        void buildSufMas() {

            calcStartField();

            for (int step = 0; (1<<step) < lenStr; ++step) {

                for (int i = 0; i < lenStr; ++i) {
                    sortPair[i] = sufMas[i] - (1<<step);
                    if (sortPair[i] < 0) {
                        sortPair[i] += lenStr;
                    }
                }

                memset (numberElement, 0, classes * sizeof(int));

                for (int indexSortPair = 0; indexSortPair < lenStr; ++indexSortPair) {
                    ++numberElement[ordClass[sortPair[indexSortPair]]];
                }

                for (int indexNumberElement = 1; indexNumberElement < classes; ++indexNumberElement) {
                    numberElement[indexNumberElement] += numberElement[indexNumberElement - 1];
                }

                for (int indexSortPair = lenStr-1; indexSortPair >= 0; --indexSortPair) {
                    sufMas[--numberElement[ordClass[sortPair[indexSortPair]]]] = sortPair[indexSortPair];
                }

                newOrdClass[sufMas[0]] = 0;
                classes = 1;

                for (int indexSufMas = 1; indexSufMas < lenStr; ++indexSufMas) {
                    int mid1 = (sufMas[indexSufMas] + (1<<step)) % lenStr;
                    int mid2 = (sufMas[indexSufMas - 1] + (1<<step)) % lenStr;
                    if (ordClass[sufMas[indexSufMas]] != ordClass[sufMas[indexSufMas-1]]
                        || ordClass[mid1] != ordClass[mid2]) {
                        ++classes;
                    }
                    newOrdClass[sufMas[indexSufMas]] = classes - 1;
                }

                memcpy (ordClass, newOrdClass, lenStr * sizeof(int));
            }
        }

        int calcSumLCP() {
            vector<int> rank(lenStr);
            for (int indexSufMas = 0; indexSufMas < lenStr; indexSufMas++) {
                rank[sufMas[indexSufMas]] = indexSufMas;
            }
            int sum = 0, commonPart = 0;
            for (int indexRank = 0; indexRank < lenStr; indexRank++) {
                if (rank[indexRank] == lenStr - 1) {
                    continue;
                }
                int next = sufMas[rank[indexRank] + 1];
                while (((indexRank + commonPart) != lenStr) && ((next + commonPart) != lenStr) &&
                       (str[indexRank + commonPart] == str[next + commonPart])) {
                    commonPart++;
                }
                sum += commonPart;
                if (commonPart > 0) {
                    --commonPart;
                }
            }
            return sum;
        }

    private:

        void calcStartField() {
            memset (numberElement, 0, ALPHABET_SIZE * sizeof(int));

            for (int indexStr = 0; indexStr < lenStr; ++indexStr) {
                ++numberElement[str[indexStr]];
            }

            for (int indexNumberElement = 1; indexNumberElement < ALPHABET_SIZE; ++indexNumberElement) {
                numberElement[indexNumberElement] += numberElement[indexNumberElement - 1];
            }

            for (int indexStr = 0; indexStr < lenStr; ++indexStr) {
                sufMas[--numberElement[str[indexStr]]] = indexStr;
            }

            ordClass[sufMas[0]] = 0;
            classes = 1;

            for (int i = 1; i < lenStr; ++i) {
                if (str[sufMas[i]] != str[sufMas[i-1]]) {
                    ++classes;
                }
                ordClass[sufMas[i]] = classes - 1;
            }
        }
};

void readInput(int &lengthKey, string &encryptStr) {
    cin >> lengthKey;
    cin >> encryptStr;
}

void calcAnswer(vector<int> &resultArray, const int lengthKey, const string &encryptStr) {
    int numberSubstring = lengthKey * (lengthKey + 1) / 2;
    string shiftStr;
    for (int startShift = 0; startShift < encryptStr.length(); ++startShift) {
        shiftStr = "";
        int endShift = startShift + lengthKey;
        for (int indexShift = startShift; indexShift < endShift; ++indexShift) {
            shiftStr += encryptStr[indexShift % encryptStr.length()];
        }
        shiftStr += "$";

        SufMas sufMas(shiftStr);
        sufMas.buildSufMas();
        resultArray.push_back(numberSubstring - sufMas.calcSumLCP());
    }
}

void printAnswer(const vector<int> &resultArray) {
    for (int currentIndex = 0; currentIndex < resultArray.size(); ++currentIndex) {
        cout<<resultArray[currentIndex]<<" ";
    }
}

int main() {
    int lengthKey;
    string encryptStr;

    //freopen("1.in", "r", stdin);
    readInput(lengthKey, encryptStr);

    vector<int> resultArray;
    calcAnswer(resultArray, lengthKey, encryptStr);
    printAnswer(resultArray);

    return 0;
}
