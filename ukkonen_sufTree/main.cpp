#include <iostream>
#include <vector>
#include <map>
#include <climits>
#include <stdlib.h>
#include <algorithm>

using namespace std;

class SufTree {
    typedef map<char, int> vertexCharMap;
    typedef vertexCharMap::const_iterator mapIt;

    const static int ROOT = 0, CREATE_FORK = 2, PASS_EDGE = 3, NOT_CALC = -1;

    struct Position {
        int vertex;
        int distToChild;

        Position() {}
        Position(int v, int up) : vertex(v), distToChild(up) { }
    };

    struct Node {
        int left;
        int right;
        int parent;
        int link;
        vertexCharMap go;
        Node() : link(NOT_CALC), left(0), right(0), parent(0) { }
    };

    vector<Node> sufTree;
    string sample;
    Position pos;
    int activePoint;
    vector<int> level; // удаленность вершины от корня
    vector<bool> flag; // true - вершина соответствует первой строке, false - второй

    public:

        SufTree(const string &s) : sample(s) {
            sufTree.push_back(Node());
            sufTree[0].link = ROOT;
            pos.distToChild = 0;
            pos.vertex = ROOT;
            activePoint = 0;
        }

        void runBuild() {
            for (int lenPrefix = 1; lenPrefix <= sample.length(); ++lenPrefix) {
                addPrefix(lenPrefix);
            }
            level.resize(sufTree.size());
            flag.resize(sufTree.size());

            level[ROOT] = 0;
            calcLevel(ROOT);

            for (int i = 0; i < flag.size(); ++i) {
                flag[i] = false;
            }

            calcFlag(ROOT);
        }

        string findMaxCommonSubstring() {
            int maxLevel = NOT_CALC;
            Node suitableNode;
            string commonSubstring("");
            for (int node = 0; node < sufTree.size(); ++node) {
                bool includeFirstString = false;
                bool includeSecondString = false;
                for (mapIt it = sufTree[node].go.begin(); it != sufTree[node].go.end(); ++it) {
                    if (flag[it->second]) {
                        includeFirstString = true;
                    } else {
                        includeSecondString = true;
                    }
                }
                if (includeFirstString && includeSecondString && level[node] > maxLevel) {
                    maxLevel = level[node];
                    suitableNode = sufTree[node];
                }
            }

            if (maxLevel != NOT_CALC) {
                int endMaxSubstring = suitableNode.right;
                for (int currentIndex = endMaxSubstring - maxLevel; currentIndex < endMaxSubstring; ++currentIndex) {
                    commonSubstring += sample[currentIndex];
                }
            }
            return commonSubstring;
        }

    private:

        Position getLink(Position cur) {
            int vertex = cur.vertex;
            int dist = cur.distToChild;
            int delta = sufTree[vertex].right - sufTree[vertex].left - dist;

            if (vertex == ROOT || (sufTree[vertex].parent == ROOT && delta == 1)) { // Корень или дети корня
                return Position(0, 0);
            }

            if (dist == 0) {  // Развилка или лист
                if (!sufTree[vertex].go.empty()) { // Развилка
                    if (sufTree[vertex].link != NOT_CALC) { // Уже считали
                        return Position(sufTree[vertex].link, 0);
                    }
                }
            }

            int parent = sufTree[vertex].parent;
            int rightNew = sufTree[vertex].right - dist;
            int leftNew = sufTree[vertex].left;
            if (parent == ROOT) {
                ++leftNew;
            }
            Position res;

            Position curNew = getLink(Position(parent, 0));
            int vertexNew = curNew.vertex;
            while (true) {
                mapIt it = sufTree[vertexNew].go.find(sample[leftNew]);
                int node = it->second;
                if (sufTree[node].right - sufTree[node].left < rightNew - leftNew) {
                    leftNew += sufTree[node].right - sufTree[node].left;
                    vertexNew = node;
                } else {
                    res = Position(node, (sufTree[node].right - sufTree[node].left) - (rightNew - leftNew));
                    break;
                }
            }

            if (dist == 0) {  // Развилка или лист
                if (!sufTree[vertex].go.empty()) { // Развилка
                    if (sufTree[vertex].link == NOT_CALC) { // Не считали
                        sufTree[vertex].link = res.vertex; // Запоминаем
                    }
                }
            }

            return res;
        }

        int addSuffixOfPrefix(int shift, int len ) {

            int ind = len - 1;
            char c = sample[ind];

            if (pos.distToChild == 0) { // Стоим в настоящей вершине
                int vertex = pos.vertex; // Текущая настоящая вершина
                mapIt it = sufTree[vertex].go.find(c);
                if (it != sufTree[vertex].go.end()) { // Есть переход по ребру (3)

                    Node child = sufTree[it->second];
                    pos = Position(it->second, child.right - child.left - 1);
                    return PASS_EDGE;

                } else { // Нет перехода по ребру (1 или 2)

                    if (sufTree[vertex].go.empty() && pos.vertex != 0) { // Это лист и не корень (1)

                    } else { // Это развилка или корень (2)

                        pos = getLink(pos);

                        sufTree.push_back(Node()); // добавляем лист
                        sufTree[sufTree.size()-1].parent = vertex;
                        sufTree[sufTree.size()-1].left = ind;
                        sufTree[sufTree.size()-1].right = LONG_MAX;

                        sufTree[vertex].go[c] = sufTree.size()-1;


                        return CREATE_FORK;
                    }
                }
            } else { // Стоим во мнимой вершине
                Node child = sufTree[pos.vertex];
                if (c == sample[child.right - pos.distToChild]) { // Есть переход по мнимой вершине (3)

                    --pos.distToChild;
                    return PASS_EDGE;

                } else { // Нет перехода по мнимой вершине (2)

                    Position pos1 = getLink(pos);

                    sufTree.push_back(Node()); // добавляем промежуточную вершину
                    sufTree[sufTree.size()-1].parent = child.parent;
                    sufTree[sufTree.size()-1].left = child.left;
                    sufTree[sufTree.size()-1].right = child.right - pos.distToChild;
                    sufTree[sufTree.size()-1].go[c] = sufTree.size();
                    sufTree[sufTree.size()-1].go[sample[child.right - pos.distToChild]] = pos.vertex;

                    sufTree[child.parent].go[sample[child.left]] = sufTree.size() - 1;

                    sufTree.push_back(Node()); // добавляем лист
                    sufTree[sufTree.size()-1].parent = sufTree.size() - 2;
                    sufTree[sufTree.size()-1].left = ind;
                    sufTree[sufTree.size()-1].right = LONG_MAX;

                    sufTree[pos.vertex].left = child.right - pos.distToChild; // меняем left у сына
                    sufTree[pos.vertex].parent = sufTree.size() - 2;

                    if (pos1.vertex==pos.vertex) {
                        pos1.vertex = sufTree.size() - 2;
                        pos1.distToChild = 1;
                    }
                    pos = pos1;

                    return CREATE_FORK;
                }
            }
        }

        void addPrefix(int len) {
            for (int shift = activePoint; shift < len; ++shift) {
                int res = addSuffixOfPrefix(shift, len);
                if (res == PASS_EDGE) {
                    activePoint = shift;
                    return;
                }
            }
            activePoint = len;
        }

        void calcLevel(int v) {
            for (mapIt it=sufTree[v].go.begin();it!=sufTree[v].go.end();++it) {
                int rebro = sufTree[it->second].right - sufTree[it->second].left;
                level[it->second] = level[v] + rebro;
                calcLevel(it->second);
            }
        }

        void calcFlag(int v) {
            for (mapIt it = sufTree[v].go.begin(); it != sufTree[v].go.end(); ++it) {
                calcFlag(it->second);
                if (flag[it->second]) {
                    flag[v] = true;
                }

            }
            int n = sample.length() / 2 - 1;
            if (n < sufTree[v].right && n>= sufTree[v].left) {
                flag[v] = true;
            }
        }
};

void readInput(string &str1, string &str2) {
    int length;
    cin>>length;
    cin>>str1;
    cin>>str2;
}

void writeOutput(const string &output) {
    cout<<output;
}

int main() {
    string str1, str2;

    //freopen("1.in","r",stdin);
    readInput(str1, str2);

    SufTree sufTree(str1 + '$' + str2 + '#');
    sufTree.runBuild();
    string maxCommonSubstring = sufTree.findMaxCommonSubstring();

    writeOutput(maxCommonSubstring);

    return 0;
}
