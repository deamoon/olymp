#include <iostream>
#include <vector>
#include <climits>
#include <stdlib.h>
#include <algorithm>

using namespace std;

//typedef map<char, int> vertexCharMap;
//typedef vertexCharMap::const_iterator mapIt;

const int ARRAYMAX = 27;

struct Position {
    int vertex;
    int distToChild;

    Position() {}
    Position(int v, int up) : vertex(v), distToChild(up) { }
};

struct Node {
    int left;
    int right;
    int parent;
    int link;
    int level;
    //vertexCharMap go;
    int go[ARRAYMAX];
    Node() : link(-1), left(0), right(0), parent(0) { for(int i=0;i<ARRAYMAX;++i) {
            go[i] = -1;
        }
    }
    //Node(int v) : vertex(v) { }

};

class SufTree {
    vector<Node> sufTree;
    vector<char> sample;
    Position pos;
    int ap;
    int ROOT;
    vector<int> level;


    public :

    SufTree() {
        ROOT = 0;

        sufTree.push_back(Node());
        sufTree[0].link = ROOT;
        sufTree[0].level = 0;
        //sample = s;
        pos.distToChild = 0;
        pos.vertex = ROOT;
        ap = 0;
    }

    Position getLink(Position cur) {
        int vertex = cur.vertex;
        int dist = cur.distToChild;
        int delta = sufTree[vertex].right - sufTree[vertex].left - dist;
        //printf("delta %d\n", delta);
        if (vertex == ROOT || (sufTree[vertex].parent == ROOT && delta == 1)) { // Корень или дети корня
            return Position(0, 0);
        }

        int parent = sufTree[vertex].parent;

        if (dist == 0) {  // Развилка или лист
            if (sufTree[vertex].right > LONG_MAX/2) { // Лист

            } else { // Развилка
                if (sufTree[vertex].link == -1) {

                } else {
                    return Position(sufTree[vertex].link, 0);
                }
            }
        }

        int r1 = sufTree[vertex].right - dist;
        int l1 = sufTree[vertex].left;
        if (parent == ROOT) {
            ++l1;
        }
        Position res;

        Position cur2 = getLink(Position(parent, 0));
        int vertex2 = cur2.vertex;
        while (true) {
            //mapIt it = sufTree[vertex2].go.find(sample[l1]);
            //printf("\nsample[l1] = %c\n", sample[l1]);
            int v = sufTree[vertex2].go[(int)sample[l1] - (int)'A'];
            if (sufTree[v].right - sufTree[v].left < r1 - l1) {
                l1 += sufTree[v].right - sufTree[v].left;
                vertex2 = v;
            } else {
                res = Position(v, (sufTree[v].right - sufTree[v].left) - (r1 - l1));
                break;
            }
        }

        if (dist == 0) {  // Развилка или лист
            if (sufTree[vertex].right > LONG_MAX/2) { // Лист

            } else { // Развилка
                if (sufTree[vertex].link == -1) {
                    sufTree[vertex].link = res.vertex;
                } else {

                }
            }
        }

        return res;
    }

    int addSuffixOfPrefix(int len, char c ) {

        int ind = len-1;
        //char c = sample[ind];

        if (pos.distToChild == 0) { // Стоим в настоящей вершине
            int vertex = pos.vertex; // Текущая настоящая вершина
            //mapIt it = sufTree[vertex].go.find(c);
            if (-1 != sufTree[vertex].go[(int)c - (int)'A']) { // Есть переход по ребру (3)
                int itS = sufTree[vertex].go[(int)c - (int)'A'];
                Node child = sufTree[itS];
                pos = Position(itS, child.right - child.left - 1);
                return 3;

            } else { // Нет перехода по ребру (1 или 2)

                if (sufTree[vertex].right > LONG_MAX/2 && pos.vertex != 0) { // Это лист и не корень (1)

                } else { // Это развилка или корень (2)

                    pos = getLink(pos);
                    //printf("pos %d %d\n", pos.vertex, pos.distToChild);

                    sufTree.push_back(Node()); // добавляем лист
                    sufTree[sufTree.size()-1].parent = vertex;
                    sufTree[sufTree.size()-1].left = ind;
                    sufTree[sufTree.size()-1].right = LONG_MAX;
                    //sufTree[sufTree.size()-1].level = vertex.level + 1;

                    sufTree[vertex].go[(int)c-(int)'A'] = sufTree.size()-1;


                    return 2;
                }
            }
        } else { // Стоим во мнимой вершине
            Node child = sufTree[pos.vertex];
            if (c == sample[child.right - pos.distToChild]) { // Есть переход по мнимой вершине (3)

                --pos.distToChild;
                return 3;


            } else { // Нет перехода по мнимой вершине (2)

                //printf("\n\npos %d %d\n", pos.vertex, pos.distToChild);
                Position pos1 = getLink(pos);
                //printf("pos1 %d %d\n\n", pos1.vertex, pos1.distToChild);

                sufTree.push_back(Node()); // добавляем промежуточную вершину
                sufTree[sufTree.size()-1].parent = child.parent;
                sufTree[sufTree.size()-1].left = child.left;
                sufTree[sufTree.size()-1].right = child.right - pos.distToChild;
                sufTree[sufTree.size()-1].go[(int)c-(int)'A'] = sufTree.size();
                sufTree[sufTree.size()-1].go[sample[child.right - pos.distToChild]-(int)'A'] = pos.vertex;

                sufTree[child.parent].go[sample[child.left]-(int)'A'] = sufTree.size()-1;

                sufTree.push_back(Node()); // добавляем лист
                sufTree[sufTree.size()-1].parent = sufTree.size()-2;
                sufTree[sufTree.size()-1].left = ind;
                sufTree[sufTree.size()-1].right = LONG_MAX;

                sufTree[pos.vertex].left = child.right - pos.distToChild; // меняем left у сына
                sufTree[pos.vertex].parent = sufTree.size()-2;

                pos = pos1;
                return 2;
            }
        }
    }

    void addPrefix(int len, char c) {
        for (int shift = ap; shift < len; ++shift) {
            int res = addSuffixOfPrefix(len, c);
            //cout<<res<<" ";
            if (res == 3) {
                ap = shift;
                return;
            }
        }
        ap = len;
        //cout<<"\n";
    }

    void runBuild(int n) {
        char c;
        for (int lenPrefix = 1; lenPrefix <= n; ++lenPrefix) {
            c = getchar();
            sample.push_back(c);
            //cout<<c;
            //for (int i=0;i<ap;++i) cout<<"1 ";

            addPrefix(lenPrefix, c);
            //cout<<"\n";
            //dfs();
        }
        level.resize(sufTree.size());
        level[ROOT] = 0;
        myDFS(ROOT);
    }

    int getLink(int vertex) {

    }

    int min(int a, int b) {
        if (a<b) return a;
        return b;
    }

    void dfs() {
        for (int i=0;i<sufTree.size();++i) {
            printf("%d %d %d %d\n", i, sufTree[i].parent, sufTree[i].left, sufTree[i].right);
            //for (mapIt it=sufTree[i].go.begin();it!=sufTree[i].go.end();++it) {
            for (int j=0;j<ARRAYMAX;++j) {
                if (sufTree[i].go[j] == -1) {
                    continue;
                }
                cout<<(char)(j + (int)'A')<<" "<<sufTree[i].go[j]<<" ";
            }
            cout<<"\n\n";
        }
        cout<<sufTree.size()<<"\n\n";
    }

    void myDFS(int v) {
        //for (mapIt it=sufTree[v].go.begin();it!=sufTree[v].go.end();++it) {
        for (int j=0;j<ARRAYMAX;++j) {
            if (sufTree[v].go[j] == -1) {
                continue;
            }
            int itS = sufTree[v].go[j];
            //int rebro = min(sufTree[it->second].right, sample.length()) - sufTree[it->second].left;
            int rebro = sufTree[itS].right - sufTree[itS].left;
            //printf("LR = %d %d %d\n", sufTree[v].right, sufTree[v].left);
            level[itS] = level[v] + rebro;
            myDFS(itS);
        }
    }

    int levelFromRoot(Position p) {
        return level[p.vertex] - p.distToChild;
    }

    void ahoKaras(int n) {
        //int v = ROOT;
        Position p(ROOT, 0);
        int i=0, old=-1;
        int max = 0;
        int index = -1;
        int index2 = -1;
        Position inpos;
        char c;
        while (i<n) {
            if (old != i) {
                c = getchar();
                old=i;
            }

            if (p.distToChild == 0) { // Стоим в настоящей вершине
                int vertex = p.vertex; // Текущая настоящая вершина
                //mapIt it = sufTree[vertex].go.find(c);
                if (-1 != sufTree[vertex].go[(int)c - (int)'A']) { // Есть переход по ребру (3)
                    int itS = sufTree[vertex].go[(int)c - (int)'A'];
                    Node child = sufTree[itS];
                    p = Position(itS, child.right - child.left - 1);
                    ++i;
                } else { // Нет перехода по ребру (1 или 2)
                    if (p.vertex == ROOT) {
                        ++i;
                    } else {
                        p = getLink(p);
                    }
                }
            } else { // Стоим во мнимой вершине
                Node child = sufTree[p.vertex];
                if (c == sample[child.right - p.distToChild]) { // Есть переход по мнимой вершине (3)
                    --p.distToChild;
                    ++i;
                } else { // Нет перехода по мнимой вершине (2)
                    p = getLink(p);
                }
            }

            int l = levelFromRoot(p);
            //printf("%d %d\n", p.vertex, sufTree[p.vertex].right - p.distToChild - sufTree[p.vertex].left);
            //printf("%d %d %d %d %d\n", p.vertex, p.distToChild, l,
            //       sufTree[p.vertex].left, sufTree[p.vertex].right - p.distToChild);
            if (l > max) {
                max = l;
                inpos = p;
                index=1;
                //index = sufTree[p.vertex].left;
                //index2 = sufTree[p.vertex].right - p.distToChild;
            }
        }

        //printf("%d %d %d\n", max, inpos.vertex, sufTree[inpos.vertex].right - inpos.distToChild);
        if (index != -1) {
            int qwe = sufTree[inpos.vertex].right - inpos.distToChild;
            for(int j=qwe-max;j<qwe;++j) {
                cout<<sample[j];
            }
        } else {

        }


    }

};



int main() {
    int n;
    string s, s1;
    freopen("1.in","r",stdin);
    cin>>n;
    //cin>>s;
    //cin>>s1;
    //
    getchar();
    SufTree sufTree;
    sufTree.runBuild(n);

    //getchar();
    //sufTree.ahoKaras(n);

    return 0;
}
